#!/usr/bin/env python
from __future__ import print_function
import sys
import os
if sys.version_info.major < 3:
    import urllib2 as request
else:
    from urllib import request

from tools.pbar import Pbar


class Reporter:
    """ Report Status during the download of files

    Attributes
    ----------
    pbar: ProgressBar
        instance of progress bar configured to indicate the downloading
        progression and status.
    """
    def __init__(self, fname):
        self.pbar = Pbar(maxval=100, desc=fname, units='Mb')

    def __call__(self, bytes_so_far, chunk_size, total_size):
        s = 1024 * 1024   # to Mb
        self.pbar.update(bytes_so_far // s, total=total_size // s)


def chunk_read(response, buf, chunk_size=8192, report_hook=None):
    """ Read a chunk of data from a response """

    total_size = response.info()

    if hasattr(total_size, 'getheader'):
        total_size = total_size.getheader('Content-Length').strip()
    else:
        total_size = total_size.get('Content-Length').strip()

    total_size = int(total_size)
    bytes_so_far = 0

    while 1:
        chunk = response.read(chunk_size)
        bytes_so_far += len(chunk)

        if not chunk:
            break
        else:
            buf.write(chunk)
        if report_hook:
            report_hook(bytes_so_far, chunk_size, total_size)

    return bytes_so_far


def urlretrieve(url, reporthook, f):
    """ Retrieve data from an url and use the reporter hooks"""
    response = request.urlopen(url)
    return chunk_read(response, f, report_hook=reporthook)


def download(urltxt, dest, fname=None):
    """ Download a file

    Parameters
    ----------

    urltxt: str
        url to download

    dest: str
        destination path and filename

    fname: str, optional
        used to simplify stdout progress

    returns
    -------
    dest: str
        final path and filename
    """
    if fname is None:
        fname = dest

    with open(dest, 'wb') as f:
        s = urlretrieve(urltxt, Reporter(fname), f)

    fmt = '%6.2f %s'
    units = ['B', 'K', 'M', 'G', 'T', 'P']
    for u in units:
        if s < 1024:
            break
        s /= 1024
    size = fmt % (s, u)
    print('\nDownloaded %s bytes' % size)

    return dest


def dl_install_all_libs(server, libs, libsdir):
    libdir = os.path.abspath(libsdir)
    if os.path.exists(libdir):
        if not os.path.isdir(libdir):
            print(libdir, 'exists and is not a directory.')
            return 1
    else:
        os.makedirs(libdir)

    if server[-1] != '/':
        server += '/'
    if libdir[-1] != '/':
        libdir += '/'
    for k in libs:
        url = server + libs[k]
        print(url)
        download(url, libdir + libs[k], libs[k])

if __name__ == '__main__':
    from config import libs_server, libs, libsdir

    print("""
            Installing libraries
          """)
    dl_install_all_libs(libs_server, libs, libsdir)
