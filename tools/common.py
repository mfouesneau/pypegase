from pylab import Formatter
import numpy as np


def latexFloat(val, precision=2.3, fmt='g'):
    """ Make a nice latex formatted string

    Attributes
    ----------
    precision: float
        numerical precision of the mantisse

    delimiter: str
        delimiter between mantisse and exponent (do not add $ symbols for tex
        macros)

    Returns
    -------
    val: str
        latex formatted string
    """
    lf = LatexFormatter(precision=precision, fmt=fmt)
    return '${0}$'.format(lf(val))


class LatexFormatter(Formatter):
    """ Make Matplotlib use nice latex formatting

    Parameters
    ----------
    precision: float
        numerical precision of the mantisse

    delimiter: str
        delimiter between mantisse and exponent (do not add $ symbols for tex
        macros)

    .. code::

        cb = plt.colorbar(im, shrink=0.9, pad=0.01, extend=extend,
                          format=LatexFormatter(precision=0.2, delimiter='\cdot'))
    """
    def __init__(self, precision=2.3, fmt='g', delimiter=r'\times'):
        self.precision = precision
        self.delimiter = delimiter
        self.fmt = fmt

    def __call__(self, f, pos=None):
        float_str = ("{0:" + str(self.precision) + self.fmt + "}").format(f)
        if "e" in float_str:
            base, exponent = float_str.split("e")
            return (r"${0} {2} 10^{{{1}}}$").format(base,
                                                    int(exponent),
                                                    self.delimiter)
        else:
            return float_str


def unsecure_histogram(a, bins, block=65536):
    """ Make a fast histogram without any check or data copy

    Parameters
    ----------
    a : array_like
        Input data. The histogram is computed over the flattened array.

    bins : int or sequence of scalars, optional
        If `bins` is an int, it defines the number of equal-width
        bins in the given range (10, by default). If `bins` is a sequence,
        it defines the bin edges, including the rightmost edge, allowing
        for non-uniform bin widths.

    Returns
    -------
    hist : array
        The values of the histogram. See `normed` and `weights` for a
        description of the possible semantics.
    bin_edges : array of dtype float
        Return the bin edges ``(length(hist)+1)``.
    """

    n = np.zeros(bins.shape, dtype=int)
    for i in np.arange(0, len(a), block):
        sa = np.sort(a[i:i+block])
        n += np.r_[sa.searchsorted(bins[:-1], 'left'),
                   sa.searchsorted(bins[-1], 'right')]
    n = np.diff(n)

    return n, bins
