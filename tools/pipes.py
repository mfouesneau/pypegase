from functools import partial, wraps, update_wrapper
from inspect import getargspec, ismethod
import itertools
import sys

try:
    import builtins
except ImportError:
    import __builtin__ as builtins


def keywords_first(f):
    """ Decorator that enables to access any argument or keyword as a keyword """
    # http://code.activestate.com/recipes/577922/ (r2)
    @wraps(f)
    def wrapper(*a, **k):
        a = list(a)
        for idx, arg in enumerate(getargspec(f).args, -ismethod(f)):  # or [0] in 2.5
            if arg in k:
                if idx < len(a):
                    a.insert(idx, k.pop(arg))
                else:
                    break
        return f(*a, **k)
    return wrapper


def kfpartial(fun, *args, **kwargs):
    """ Allows to create partial functions with arbitrary arguments/keywords """
    return partial(keywords_first(fun), *args, **kwargs)


class NameSpace(dict):
    """A dict subclass that exposes its items as attributes.
    """
    def __init__(self, name, *args, **kwargs):
        self.__name__ = name
        dict.__init__(self, *args, **kwargs)

    def __dir__(self):
        return tuple(self)

    def __repr__(self):
        names = ', '.join([k for k in dir(self) if k[0] != '_'])
        return "{s.__name__:s}: {r:s}".format(s=self, r=names)

    def __getattribute__(self, name):
        try:
            return self[name]
        except KeyError:
            msg = "'{s.__name__:s}' has no attribute '{name:s}'"
            raise AttributeError(msg.format(s=self, name=name))

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        del self[name]

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        return False


class Pipeable(object):
    """ Decorator overloading | operator (__ror__) such that you can pipe
    functions where the first argument is the variable on the left side of the
    | operator.
    This decorator allows you to use the decorated function normally and uses
    the provided values when using in pipes.

    >>> import pylab as plt
    >>> _p = Pipeable(plt.plot, color='red', linestyle='--')
    >>> _p(range(10), 'o-')  #  works
    >>> range(10) | _p      #  will plot a red dashed line

    """
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def __ror__(self, lhs):
        return self.func(lhs, *self.args, **self.kwargs)

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)

    def __repr__(self):
        return self.func.__repr__()


class Pipe(object):
    """ Decorator overloading | operator (__ror__) such that you can pipe
    functions where the first argument is the variable on the left side of the
    | operator.
    The difference with Pipeable is that you cannot use decorated function
    outside of pipes but you gain the possibility to update the calling
    parameters

    Used with keywords_first make this a powerful Task
    """
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs
        update_wrapper(self, func)

    def __ror__(self, other):
            return self.func(other, *self.args, **self.kwargs)

    def __call__(self, *args, **kwargs):
        return Pipeable(self.func, *args, **kwargs)

    def __repr__(self):
        return 'Pipe: {}'.format(self.func.__repr__())

    def __str__(self):
        return '{}'.format(self.func.__name__)


@Pipe
def groupby(iterable, keyfunc):
    return itertools.groupby(sorted(iterable, key=keyfunc), keyfunc)


@Pipe
def sort(iterable, **kwargs):
    return sorted(iterable, **kwargs)


@Pipe
def reverse(iterable):
    return reversed(iterable)


@Pipe
def null(x):
    list(x)
    return


@Pipe
def mean(iterable):
    """
    Build the average for the given iterable, starting with 0.0 as seed
    Will try a division by 0 if the iterable is empty...
    """
    total = 0.0
    qte = 0
    for x in iterable:
        total += x
        qte += 1
    if qte == 0:
        return None
    else:
        return total / qte


@Pipe
def count(iterable):
    "Count the size of the given iterable, walking thrue it."
    count = 0
    for x in iterable:
        count += 1
    return count


@Pipe
def max(iterable, **kwargs):
    return builtins.max(iterable, **kwargs)


@Pipe
def min(iterable, **kwargs):
    return builtins.min(iterable, **kwargs)


@Pipe
def ptp(iterable, **kwargs):
    return (builtins.min(iterable, **kwargs), builtins.max(iterable, **kwargs))


@Pipe
def median(iterable, **kwargs):
    return 0.5 * (builtins.min(iterable, **kwargs) + builtins.max(iterable, **kwargs))


@Pipe
def sum(x):
    return builtins.sum(x)


@Pipe
def reduce(iterable, function, initial=None):
    if initial is not None:
        return builtins.reduce(function, iterable, initial)
    else:
        return builtins.reduce(function, iterable)


@Pipe
def where(iterable, predicate):
    return (x for x in iterable if (predicate(x)))


@Pipe
def to_dict(iterable):
    return dict(iterable)


@Pipe
def to_list(iterable):
    return list(iterable)


@Pipe
def to_type(x, t):
    return t(x)


@Pipe
def to_tuple(iterable):
    return tuple(iterable)


@Pipe
def bwrite(val, fmt="%s", buffer=sys.stdout):
    buffer.write(fmt % val)


@Pipe
def tee(iterable):
    for item in iterable:
        sys.stdout.write(str(item) + "\n")
        yield item


def _fmap(val, fnseq):
    return (fk(val) for fk in fnseq)


@Pipe
def apply(vals, args=(), iter=True):
    r = _fmap(vals, args)
    if not iter:
        return list(r)
    else:
        return r


@Pipe
def enumerate(iterable):
    return builtins.enumerate(iterable)
