# AL_tests1.py      - Started June 2013
# 
#   Run this in ipython with the magic command :   %run AL_tests1.py
#
#   Contents :
#     Some trivial code to test continuous population synthesis 
#     (mostly isochrone interpolation)
#

from pypegase import imf             # directory
from pypegase import isochrones      # directory
from pypegase import stellibs        # directory
from pypegase import nebular         # nebular.py
from pypegase import ssp             # ssp.py

from pypegase import units           # directory (pint.py, __init__.py)

import numpy as np
import matplotlib.pyplot as plt

Z=0.02
#ages = np.arange(5.0118721e6,5.2480739e6,0.01e6)
#ages = np.arange(0.99999e9,1.5e9,0.01e9)
ages = np.array([5.e8])
 
oIMF = imf.Kroupa2001()
#oISO = isochrones.Padova2010()  # Doesnt work
oISO = isochrones.Pegase()
oSL = stellibs.BaSeL()
oNEB = nebular.pegase()
fracNeb=0.5


plt.figure()  
#fig = plt.figure()
#box1 = fig.add_subplot(221) # for spectra
#box2 = fig.add_subplot(222) # for HRdiag

for kk, thisAge in enumerate(ages):
  print("kk and current age = ", kk, thisAge)
  l,s,p = ssp.continuous(thisAge, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb,
                       with_units=False, inLsun=False)  
  # l contains the wavelengths in Angstroems
  # s contains the spectrum for thisAge
  #    If (with_units) : erg/s/angstrom/msun   MANQUE /cm2
  #    If (with_units and inLsun) : erg/s/angstrom/msun/lsun  PAS DE SENS !!
  #    If (with_units), use l.magnitude and s.magnitude 
  # p is a dictionary with the parameters - type p.keys() to see what
  #   it contains
  print ("----- with_units=False, inLsun=False")
  print ("l= ", l)
  print ("s= ", s)
  
  l,s,p = ssp.continuous(thisAge, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb,
                       with_units=True, inLsun=False)
  # l contains the wavelengths in Angstroems
  # s contains the spectrum for thisAge
  #    If (with_units) : erg/s/angstrom/msun   MANQUE /cm2
  #    If (with_units and inLsun) : erg/s/angstrom/msun/lsun  PAS DE SENS !!
  #    If (with_units), use l.magnitude and s.magnitude 
  # p is a dictionary with the parameters - type p.keys() to see what
  #   it contains
  print ("----- with_units=True, inLsun=False")
  print ("l= ", l)
  print ("s= ", s)

  l,s,p = ssp.continuous(thisAge, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb,
                       with_units=True, inLsun=True)
  # l contains the wavelengths in Angstroems
  # s contains the spectrum for thisAge
  #    If (with_units) : erg/s/angstrom/msun   MANQUE /cm2
  #    If (with_units and inLsun) : erg/s/angstrom/msun/lsun  PAS DE SENS !!
  #    If (with_units), use l.magnitude and s.magnitude 
  # p is a dictionary with the parameters - type p.keys() to see what
  #   it contains
  print ("----- with_units=True, inLsun=True")
  print ("l= ", l)
  print ("s= ", s)

  



  1/0

  stars = oISO._get_continuous_isochrone( thisAge, Z )
  dN = oIMF.nstars ( stars['logM'], dlogm=stars['dlogm'] )
  print("age and dN(sum)", thisAge, dN.sum() )

  r=oSL.interpMany(stars['logT'], stars['logg'], Z, stars['logL'], weights=dN )
  s0 = oSL.genSpectrum(r)
  l0 = oSL.wavelength

  ##plt.loglog(l,s,label=str(ages[kk]))
  #plt.plot(l,s,label=str(ages[kk]))
  #plt.legend(loc="lower right")
  #plt.xlim(2.e3,2.5e4)
  ##plt.ylim(1.e24)
  #plt.ylim(0.,1.e32)

  plt.plot(kk,s[800],'.')

  #box1.plot(l0,s0,label=str(ages[kk]))
  #box1.legend(loc="upper right")
  #box1.set_xlim(2.e3,2.5e4)
  ##box1.set_ylim(0.,1.e32)

  #box2.plot(stars['logT'][::80],stars['logL'][::80],'o', label=str(ages[kk]))
  #box2.legend(loc="lower left")
  #box2.set_xlim(4.2,3.3)

plt.show()

## Look into the isochrones more carefully  - works only for padova
    
#t = oISO.data.selectWhere( '(Z == _z)', condvars={'_z': Z} )
#
#plt.clf()
#for kk in np.arange(152,155): 
#  tt = t.selectWhere( '(logA == _age)', condvars={'_age': np.log10(oISO.ages[kk])} )
#  plt.plot( tt['logT'], tt['logL'], 'o', label=str(oISO.ages[kk]))
#  plt.plot( tt['logT'], tt['logL'], label=str(oISO.ages[kk]))
#  #
#  #plt.plot( np.arange(tt['logT'].size), tt['logT'], 'o', label=str(oISO.ages[kk]))
#  #plt.plot( np.arange(tt['logT'].size), tt['logT'],  label=str(oISO.ages[kk]))
#  #
#  #plt.plot( tt['logM'], tt['logT'], 'o', label=str(oISO.ages[kk]))
#  #plt.plot( tt['logM'], tt['logT'],  label=str(oISO.ages[kk]))
#
#
#plt.legend(loc="lower left")
#plt.xlim(3.9, 3.3)
#
#plt.show()

#print(tt.keys())



