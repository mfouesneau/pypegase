"""
AL_make_collection.py

Produce collections of discrete single stellar populations.

Contents :
  * save_samples (fname, p,l,s,stars) 
        Used to save the results of simple_coll


  * simple_coll (Nstars=int(1e4), Z=0.02, ages=[1.e6,1.e7,1.e8,1.e9,1.e10], 
                 fracNeb=0, nsamp=2, 
                 oIMF=oIMF, oISO=oISO, oNEB=oNEB, oSL=oSL)

  * powerlaw_coll (Nstars_min=int(1e3), Nstars_max=int(2e6), powlaw_index=-2,
                 Z=0.02, 
                 age_min=1.e6, age_max=1.4e10, 
                 oIMF=oIMF, oISO=oISO, oNEB=oNEB, oSL=oSL)

History :
  April 2017 : Started by A.Lancon. Inspired from "make_collections.py", as 
  written previously by M.Fouesneau.
"""

import numpy as np
import os   

from pypegase.io import Table
from pypegase.io.fits import pyfits    # This in fact loads astropy.io as pyfits
                                       # if astropy is available,
                                       # and pyfits otherwise.
from pypegase.config import __NTHREADS__
from pypegase import imf
from pypegase import isochrones
from pypegase import nebular
from pypegase import stellibs
from pypegase.units import unit, hasUnit
from pypegase.tools.pbar import Pbar
#from pypegase.ssp import fast_discrete1 as fast_discrete
from pypegase.ssp import fast_discrete1

from pyphot import LickLibrary


# Some defaults
oIMF     = imf.Kroupa2001()
oISO     = isochrones.Pegase()
oNEB     = nebular.pegase()
#oSL      = stellibs.BaSeL()
oSL      = stellibs.Elodie()


#-------------------------------------------------------------------
def save_samples(fname, p, l, s, stars):

    def unit_drop(q, u=None):
        """ Drop units if present """
        if hasUnit(q):
            if u is not None:
                return q.to(u).magnitude
            else:
                return q.magnitude
        else:
            return q

    data = np.vstack([unit_drop(s), unit_drop(l, 'AA')])

    header = pyfits.Header()
    header['SPEC_UNIT'] = '{0:s}'.format(str(s.units))
    header['LAMB_UNIT'] = '{0:s}'.format(str(l.units))
    pyfits.writeto(fname, data, header=header)

    data = {}
    header = {}
    for k, v in p.items():
        _data = unit_drop(v)
        if hasattr(_data, 'ndim'):
            data[k] = _data
        else:
            header[k] = _data
        if hasUnit(v):
            header['{0:s}_UNIT'.format(str(k))] = str(v.units)

    Table(data, header=header).write(fname, append=True)

#-------------------------------------------------------------------
def simple_coll (Nstars=int(1e4), Z=0.02, ages=[1.e6,1.e7,1.e8,1.e9,1.e10], 
                 fracNeb=0, nsamp=2, 
                 oIMF=oIMF, oISO=oISO, oNEB=oNEB, oSL=oSL,
                 fname_prefix='simplecoll_', clob=True):
    """ 
    simple_coll (Nstars=int(1e4), Z=0.02, ages=[1.e6,1.e7,1.e8,1.e9,1.e10], 
                 fracNeb=0, nsamp=2,
                 oIMF=oIMF, oISO=oISO, oNEB=oNEB, oSL=oSL,
                 fname_prefix='simple_', clob=True)

    Produce a collection of stochastic spectra with fast_discrete,
    at a given metallicity, for a list of ages, 
    with nsamp runs per age.
    Save them in files (one file per age).

    Input info:
      ages : requested ages for the calculations, in yr.
      fname_prefix : prefix of the output fits filenames (age and Z info
                     will be appended)
    
    """
    
    for age in Pbar('Ages').iterover(ages):

        _age = age * unit['yr']
        l, s, p, stars, stars_specs, dN  = fast_discrete1(Nstars, _age, Z, oIMF,
                                       oISO, oSL, oNEB, fracNeb=fracNeb,
                                       dlogm=0.01, nsamp=nsamp,
                                       full_outputs=True, nthreads=__NTHREADS__)
        fname = fname_prefix+'age_{0:05d}_Z_{1:5f}.fits'.format(int(age * 1e-6),Z)
        if os.path.isfile(fname) and clob:
            os.remove(fname)
            
        print(l)
        print(s)
        print(p)
        save_samples(fname, p, l, s, stars)


def simple_plot(fname='simplecoll_age_00010_Z_0.020000.fits'):
    """ 
    simple_plot(fname='simplecollage_1_Z_0.02.fits', measure_lick=True):
    This little code reads one of the fits files produced by simple_coll(), 
    and 
    and makes some figures.

    Returns :
        spectra, waves, params
    """

    from matplotlib import pylab as plt

    # Read fits file containing spectra and associated data
    hh = pyfits.open(fname)
    hh.info()
    print(repr(hh[0].header))   # lists the header ; 'repr' improves formatting
    print('--')
    print(repr(hh[1].header))
    #for key in hh[0].header.keys():  # hh[0].header.keys() is an 'iterable' 
    #                                 # at least in Python 3; you cant print it.
    #   print(key)

    spectra = hh[0].data   # spectra and wavelengths
    params = hh[1].data    # table with simulation parameters
    hh.close()

    Nspec = spectra.shape[0]-1
    Nwaves = spectra.shape[1]
    waves = spectra[Nspec,]     # Wavelengths in AA
    spectra = spectra[:Nspec,]  # Now spectra only

    # To inspect the params, type things like :
    #  print (params['Mtot'])
    #  print (params)

    for ii in range(Nspec):
        plt.plot(waves,spectra[ii,:],'b', alpha=0.2, lw=0.4, c='black')
    plt.plot(waves, spectra.mean(axis=0), 'r', lw=0.6)
    if 'BaSeL' in oSL.name:
        plt.yscale('log') and plt.xscale('log')
    plt.xlabel(r'Wavelength ($\AA$)')
    plt.ylabel('Flux')
    plt.show()


    return spectra, waves, params



#---------------------------------------------------------------------------
def powerlaw_coll (Nstars_min=int(1e3), Nstars_max=int(2e6), powlaw_index=-2,
                 Z=0.02, 
                 age_min=1.e6, age_max=1.4e10, 
                 oIMF=oIMF, oISO=oISO, oNEB=oNEB, oSL=oSL):
    print("powerlaw_coll : TO BE DONE !")


#-------------------------------------------------------------------------
def doit_simple ():

    # Define defaults  for this function
    #   NB: if you wish to measure indices, you should use the Elodie.

    oIMF     = imf.Kroupa2001()
    oISO     = isochrones.Pegase()
    oNEB     = nebular.pegase()
    #oSL      = stellibs.BaSeL()
    oSL      = stellibs.Elodie()

    Nstars=int(1e4)
    Zs = [0.01, 0.02]  # list of requested Z
    ages = [1.e6, 1.e7, 1.e8, 1.e9, 1.e10]  # list of requested ages
    fracNeb=0
