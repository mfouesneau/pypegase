""" Implements a Simple Stellar Population simulation functions
    aiming to provide easier interface to the related quantities """

import numpy as np

from .units import unit
from .helpers import val_in_unit
from .config import __NTHREADS__
from .tools.pbar import Pbar
from .tools.common import unsecure_histogram as histogram

Lsun = float(unit['lsun'].to('erg/s').magnitude)


def continuous(age, Z, imf, iso, sl, neb, fracNeb=0., **kwargs):
    """
    Generate an SSP assuming a continuously sampled mass function

    Parameters
    ----------

    age: float
        age of the population (in yr)

    Z: float
        metallicity

    imf: imf.Imf instance
        Initial Mass function

    iso: isochrone.Isochrone instance
        Isochrone library

    sl: stellib.Stellib instance
        stellar library

    neb: nebular.Nebular instance
        Nebular Model

    fracNeb: float
        Photon fraction tranformed into nebular component

    Returns
    -------
    l: numpy.ndarray[float, ndim=1]
        wavelength in AA

    s: numpy.ndarray[float, ndim=1]
        Spectrum in ergs/s/AA/msun

    p: dict
        various intermediate results and population properties
    """
    # check units
    _age = val_in_unit('age', age, 'yr')

    # generate a sample of stars using a continously sampled isochrone
    stars = iso.get_continuous_isochrone(_age, Z)

    # compute the contribution of each isochrone point
    # given the IMF and continuous assumption
    # i.e., integrate the number of stars per mass bin
    # dN is a fractional number of stars for 1 Msun population
    stars_dN = imf.nstars1(stars['logM'])   # -- nstar1 for debug

    # generate a stellar spectrum for each "star" point
    # from the isochrone sampling.
    stars_spectra = sl.generate_individual_spectra(stars)

    # stars_spectra contains the spectra, their wavelength 
    # but may also contain library indexes is one use
    # a composite library (lib1 + lib2 + ...)
    # I silently unpack the result below.
    try: 
        # Composite library also returns which lib was used
        l0, s0, _ = stars_spectra
    except ValueError:
        # Single library
        l0, s0 = stars_spectra

    # Sum the spectra weighted by their (number) contribution
    stellar_spectrum = np.nansum(s0 * stars_dN[:, None], 0)

    # We now compute the nebular component by first computing the fraction 
    # of photons ionizing HI per star.
    NHI_per_star = sl.generate_individual_values(stars, 'NHI')
    # we sum the fractions weighted by their contribution
    NHI_total = np.nansum(NHI_per_star * stars_dN)
    # finally we add the nebular component to the stellar one
    # given the fraction `fracNeb` of NHI that is re-processed
    if (neb is not None) & (fracNeb > 0):
        l, s = neb.add_emission(l0, stellar_spectrum, NHI_total, fracNeb)
    else:
        l, s = l0, stellar_spectrum

    # let's propagate units from the stellar library
    s = s * sl.flux_units / unit['msun']

    # one can extract some properties of the population
    properties = dict(
        # population
        Nstars=0,                  # continuous convention
        Mtot=1. * unit['Msun'],    # by definition continuous populations
        Lbol=(10 ** stars['logL']).sum() * unit['lsun'],
        l0=l0,
        s0=stellar_spectrum * sl.flux_units / unit['msun'],
        # ingredients
        stellib=sl,
        iso=iso,
        neb=neb,
        imf=imf,
        # parameters
        fracneb=fracNeb,
        age=age,
        Z=Z,
        # intermediate
        stars=stars,
        dN=stars_dN
    )

    return l, s, properties


def fast_discrete(nstars, age, Z, imf, iso, sl, neb, fracNeb=0., dlogm=0.001, nsamp=20, **kwargs):
    """
    Generate an SSP assuming a continuously sampled mass function and
    boostrap the mass binning according to the number of stars.
    The number of samples is set by nsamp

    The approximation we use in this modeling scheme is that stars are
    binned in mass to their continuous equivalent mass bins and represented
    by their central star spectrum.  This approximation is good enough as
    long as variations from on side of the mass bin to the other are
    negligeable.

    This function is optimized to make large collections of clusters at fixed
    isochrone.

    Parameters
    ----------
    nstars: int
        total number of stars in the population

    age: float
        age of the population (in yr)

    Z: float
        metallicity

    imf: imf.Imf instance
        Initial Mass function

    iso: isochrone.Isochrone instance
        Isochrone library

    sl: stellib.Stellib instance
        stellar library

    neb: nebular.Nebular instance
        Nebular Model

    fracNeb: float
        Photon fraction tranformed into nebular component

    nsamp: int
        number of mass sampling

    Returns
    -------
    l: numpy.ndarray[float, ndim=1]
        wavelength in AA

    s: numpy.ndarray[float, ndim=1]
        Spectrum in ergs/s/AA or lsun/AA

    p: dict
        various population properties

    stars: Table
        table of stars describing the isochrone
        only if full_output is set

    specs: ndarray
        spectra associated to each star in stars
        only if full_output is set

    dN: ndarray
        number of stars represented by each of the ones in stars
        only if full_output is set

    """
    # check units
    _age = val_in_unit('age', age, 'yr').magnitude

    # generate a sample of stars using a continously sampled isochrone
    # also retrieve the bins for further down usage
    stars, logm_bins = iso.get_continuous_isochrone(age, Z, ret_bins=True, dlogm=dlogm)
    m_bins = 10 ** logm_bins

    # try to add robustness
    if (np.diff(m_bins) < 0).any():
        raise Exception('binning error')

    # get the predicted number of stars per bin of mass on this isochrone
    # dN = imf.nstars(stars['logM'], ret_bins=False, edges=logm_bins)
    maxM = 10 ** stars['logM'].max()
    
    # generate a stellar spectrum for each "star" point
    # from the isochrone sampling.
    stars_spectra = sl.generate_individual_spectra(stars)
    
    # We now compute the nebular component by first computing the fraction 
    # of photons ionizing HI per star.
    # FIXME: check composite lib return
    NHI_per_star = sl.generate_individual_values(stars, 'NHI')
    
    # stars_spectra contains the spectra, their wavelength 
    # but may also contain library indexes is one use
    # a composite library (lib1 + lib2 + ...)
    # I silently unpack the result below.
    try: 
        # Composite library also returns which lib was used
        l0, s0, _ = stars_spectra
    except ValueError:
        # Single library
        l0, s0 = stars_spectra
        
    # Random samples will now affect the rest
    # 
    # dN is a the number of stars per mass bin
    # let's draw Nstar masses and put them into the isochrone bins
    stars_masses = imf.random(nstars)
    stars_dN = np.histogram(stars_masses, bins=m_bins)[0]
    # Sum the spectra weighted by their (number) contribution
    stellar_spectrum = np.nansum(s0 * stars_dN[:, None], 0)
    # we sum the fractions weighted by their contribution
    NHI_total = np.nansum(NHI_per_star * stars_dN)
    # finally we add the nebular component to the stellar one
    # given the fraction `fracNeb` of NHI that is re-processed
    if (neb is not None) & (fracNeb > 0):
        l, s = neb.add_emission(l0, stellar_spectrum, NHI_total, fracNeb)
    else:
        l, s = l0, stellar_spectrum

    # setting up book keeping variables
    specs = np.zeros((nsamp, s.size), dtype=float)
    Mtot = np.zeros(nsamp, dtype=float)
    Minit = np.zeros(nsamp, dtype=float)
    Mdead = np.zeros(nsamp, dtype=float)
    Mmax = np.zeros(nsamp, dtype=float)
    Lbol = np.zeros(nsamp, dtype=float)
    dN = np.zeros((nsamp, len(stars)), dtype=float)
    age = np.ones(nsamp, dtype=float) * _age
    Z = np.ones(nsamp, dtype=float) * Z
    
    # some properties of the population
    specs[-1] = s
    Lbol[-1] = (10 ** stars['logL']).sum()
    Mtot[-1] = np.sum(stars_masses)
    Minit[-1] = np.sum(stars_masses)       # mass contributing to the light
    Mmax[-1] = np.max(stars_masses)        # maximum single stellar mass 
    ind = (stars_masses > maxM)       
    Mdead[-1] = np.sum(stars_masses[ind])  # total mass above the isochrone limit
    dN[-1] = stars_dN

    for k in Pbar('Mixing').iterover(range(nsamp - 1)):
        _masses = imf.random(nstars)
        stars_masses = imf.random(nstars)
        stars_dN = np.histogram(stars_masses, bins=m_bins)[0]
        stellar_spectrum = np.nansum(s0 * stars_dN[:, None], 0)
        NHI_total = np.nansum(NHI_per_star * stars_dN)
        if (neb is not None) & (fracNeb > 0):
            l, s = neb.add_emission(l0, stellar_spectrum, NHI_total, fracNeb)
        else:
            l, s = l0, stellar_spectrum
            
        specs[k] = s
        Lbol[k] = (10 ** stars['logL']).sum()
        Mtot[k] = np.sum(stars_masses)
        Minit[k] = np.sum(stars_masses)       # mass contributing to the light
        Mmax[k] = np.max(stars_masses)        # maximum single stellar mass 
        ind = (stars_masses > maxM)       
        Mdead[k] = np.sum(stars_masses[ind])  # total mass above the isochrone limit
        dN[k] = stars_dN
        
    properties = dict(
        # populations
        Nstars=nstars, # continuous convention
        Lbol=Lbol * unit['lsun'],
        Mmax=Mmax * unit['Msun'],
        Minit=Minit * unit['Msun'],
        Mtot=Minit * unit['Msun'],
        Mdead=Mdead * unit['Msun'],
        dN=dN,
        # ingredients
        stellib=sl,
        iso=iso,
        neb=neb,
        imf=imf,
        # parameters
        fracneb=fracNeb,
        age=age,
        Z=Z,
        # intermediate
        stars=stars
    )
    specs = specs * sl.flux_units 

    return l, specs, properties

fast_discrete1 = fast_discrete
