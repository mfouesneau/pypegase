from pypegase.tools import figrc
from pypegase import stellibs
import pylab as plt
figrc.ezrc(18, 1, 20)

basel = stellibs.BaSeL()
rauch = stellibs.Rauch()

dlogT = 0.1
dlogg = 0.25
basel.plot_boundary(alpha=0.3, color='b', dlogT=dlogT, dlogg=dlogg, label='BaSeL')
rauch.plot_boundary(alpha=0.3, color='g', dlogT=dlogT, dlogg=dlogg, label='Rauch')

plt.plot(basel.grid.logT, basel.grid.logg, 'b.')
plt.plot(rauch.grid.logT, rauch.grid.logG, 'g.')

plt.legend(frameon=False, loc='best')
plt.xlim(5.4, 3.2)
plt.ylim(8.4, -1.4)
plt.xlabel('log$_{10}$(T$_{\\rm{eff}}$ / K)')
plt.ylabel('log$_{10}$(g / cm.s$^{-2}$)')
