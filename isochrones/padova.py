"""
Isochrone class

Intent to implement a generic module to manage isochrone mining from various
sources.
"""
from __future__ import print_function
import numpy as np
from numpy import interp

from ..helpers import val_in_unit
from ..io import Table
from ..config import localpath
from .isochrone import Isochrone


class Padova2010(Isochrone):
    def __init__(self):
        self.name = 'Padova 2010 (Marigo 2008 + Girardi 2010)'
        self.source = localpath + '/libs/padova2010.iso.fits'
        self._load_table_(self.source)
        self.ages = 10 ** np.unique(self.data['logA'])
        self.Z    = np.unique(self.data['Z'])

    def _load_table_(self, source):
        self.data = Table(self.source)
        # Alias columns
        _lst = [('logg','logG'), ('logT','logTe'), ('logL','logL/Lo'),
                ('logA','log(age/yr)'),]
        for k, v in _lst:
            self.data.set_alias(k, v)

        self.data.add_column('logM',  np.log10(self.data['M_ini']))
        self.data.add_column('age',  np.log10(self.data['logA']))

        self.data.header['NAME'] = 'Isochrone from %s' % self.name

    def get_isochrone(self, age, metal=None, FeH=None, masses=None, *args,
                      **kwargs):
        """ Generate an isochrone from the library

        Parameters
        ----------
        age: float, optional units
            age of the sequence. Assuming age in years if not associated with
            units.

        metal: float
            metallicity Z (e.g., Zsun = 0.02)

        FeH: float
            Chemical abundance normalized to solar abundance
            (e.g., [Fe/H]_sun = -4.33 )

        masses: ndarray, float, optional
            masses at which resample the isochrone

        .. note::

            do not provide both metal and FeH. Only metal will be used.

            No interpolation in metallicity yet
        """
        _age = val_in_unit('age', age, 'yr').magnitude

        if ((metal is None) & (FeH is None)):
            raise ValueError("Need a chemical parameter value (metal or FeH")

        if (metal is not None) & (FeH is not None):
            print("Warning: both Z & [Fe/H] provided, ignoring [Fe/H].")

        if metal is None:
            metal = self.FeHtometal(FeH)

        if metal not in self.Z:
            print('Metalicity inpterpolation not implemented')
            raise ValueError("Metal %f not find in %s" % (metal, self.Z))

        if _age in self.ages:
            # no interpolation, isochrone already in the file
            data = self.data.selectWhere('*', '(Z == {metal:f}) & (age == {age:f})'.format(age=_age, metal=metal))
            # make sure you have ordered masses (by def of isochrone)
            data.sort('logM')
        else:
            # interpolate between isochrones
            d = (self.ages - float(_age)) ** 2
            a1, a2 = np.sort(self.ages[np.argsort(d)[:2]])

            if 'silent' not in kwargs:
                print("Warning: Interpolation between %d and %d Myr" % (a1, a2))

            r = np.log10(_age / a1) / np.log10(a2 / a1)

            t1 = self.data.selectWhere('*', '(Z == {metal:f}) & (age == {age:f})'.format(age=a1, metal=metal))
            t2 = self.data.selectWhere('*', '(Z == {metal:f}) & (age == {age:f})'.format(age=a2, metal=metal))
            stop = min(t1.nrows, t2.nrows)

            # make sure you have ordered masses (by def of isochrone)
            t1.sort('logM')
            t2.sort('logM')

            data = {}
            for kn in t1.keys():
                y2 = t2[kn][:stop]
                y1 = t1[kn][:stop]
                data[kn] = y2 * r + y1 * (1. - r)
                del y1, y2
            data = Table(data)

        # mass selection
        if masses is not None:
            # masses are expected in logM for interpolation
            _m = np.log10(val_in_unit('masses', masses, 'Msun').magnitude)
            data_logM = data['logM']
            _data = {}
            for kn in data:
                _data[kn] = interp(_m, data_logM, data[kn])

            data = Table(data)

        data.header['NAME'] = 'Isochrone from %s' % self.name
        data.header['metal'] = metal
        data.header['time'] = _age
        return data

    _get_isochrone = get_isochrone
