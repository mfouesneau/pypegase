"""
Isochrone class

Intent to implement a generic module to manage isochrone mining from various
sources.
"""
from __future__ import print_function
import numpy as np
from ..io import Table


class Isochrone(object):
    def __init__(self, name='', *args, **kwargs):
        self.name = name

    def metalToFeH(self, metal):
        """ Convert Z to [Fe/H] values

        Zsun = 0.02 <-> [Fe/H]sun = -4.33
        Z = [ 0.0004, 0.004, 0.008, 0.02, 0.05 ]
        [Fe/H] = [ -1.7  , -0.7 , -0.4 , 0   , 0.4 ]

        .. math::

            [Fe/H] = log10( Z / Z_{\odot})

        Parameters
        ----------
        metal: float
            metallicity Z

        Returns
        -------
        val: float
            [Fe/H] concentration
        """
        return np.log10(float(metal) / 0.02)

    def FeHtometal(self, feh):
        """ Convert Z to [Fe/H] values

        Zsun = 0.02 <-> [Fe/H]sun = -4.33
        Z = [ 0.0004, 0.004, 0.008, 0.02, 0.05 ]
        [Fe/H] = [ -1.7  , -0.7 , -0.4 , 0   , 0.4  ]

        .. math::

            Z = 10 ^ {[Fe/H]} * Z_{\odot}

        Parameters
        ----------
        val: float
            [Fe/H] concentration

        Returns
        -------
        metal: float
            metallicity Z
        """
        return 10 ** float(feh) * 0.02

    def get_isochrone(self, *args, **kwargs):
        """ Retrieve isochrone from the original source
            internal use to adapt any library
        """
        raise NotImplementedError

    def get_continuous_isochrone(self, *args, **kwargs):
        """ Return a resampled isochrone accounting for variations
            useful for continuous sampling

        Parameters
        ----------
        dlogm: float, optional, default=0.01
            log mass step used to sample the isochrone

        dlogt: float, optional, default=0.01
            log temperature used to sample the isochrone

        dlogl: float, optional, default=0.01
            log luminosity used to sample the isochrone

        epsilon: float, optional, default=0.0
            shift samples by espilon when variations in mass are null.

        Other parameters are forwarded to `:func:get_isochrone`

        Returns
        -------
        tab: Table
            Table containing the isochrone samples and information per point
        """
        # define the maximum allowable difference between points
        dlogm = kwargs.pop('dlogm', 0.01)
        dlogt = kwargs.pop('dlogt', 0.01)
        dlogl = kwargs.pop('dlogl', 0.01)
        epsilon = kwargs.pop('epsilon', 0. )  # 1e-9)

        iso = self.get_isochrone(*args, **kwargs)
        logT, logg, logL, logM = iso['logT'], iso['logg'], iso['logL'], iso['logM']
        Z = iso['Z']

        # compute vector of discrete derivatives for each quantity
        # and the final number of points
        npts  = (np.abs(np.divide(np.diff(logM), dlogm))).astype(int)
        npts += (np.abs(np.divide(np.diff(logT), dlogt))).astype(int)
        npts += (np.abs(np.divide(np.diff(logL), dlogl))).astype(int)
        idx = np.hstack([[0], np.cumsum(npts + 1)])
        # idx = np.hstack([[0], np.cumsum(npts)])
        # set up vectors for storage. We drop the last value corresponding to
        # an edge but storing centers only
        ntot  = (npts + 1).sum()
        # note all quantities below will be in log scale
        newm  = np.zeros(ntot, dtype=float)
        newdm = np.zeros(ntot, dtype=float)
        newt  = np.zeros(ntot, dtype=float)
        newg  = np.zeros(ntot, dtype=float)
        newl  = np.zeros(ntot, dtype=float)

        for i in range( len(npts) ):
            a, b = idx[i], idx[i] + npts[i] + 1
            # check if the variations lead to adding points

            lMi = logM[i]
            lMj = logM[i + 1]
            _dl = lMj - lMi

            # Mass is tricky because it is used also to compute the number
            # of stars per bin of mass.
            # So that we check that the isochrones do not provide
            # duplicated masses (should not, in principle) and in case of
            # identical values, move the mass by epsilon (<1e-5 is
            # required)

            if abs(_dl) < epsilon:
                lMj += epsilon
                logM[i + 1:] += epsilon  # propagate the shifts

            if npts[i] > 0:
                # construct new 1d grids in each dimension, being careful about endpoints
                # append them to storage vectors

                # Note: the initial version of pegase computes the edges of all
                #      bins instead of the central points.
                #      in the following, we shift the values by 1/2 the dlog(X)
                #      to correct for that and return to a more natural
                #      definition.
                _dlogm = (lMj - lMi) / (npts[i] + 1)
                newm[a:b]  = np.linspace(lMi, lMj, npts[i] + 1, endpoint=False)
                newdm[a:b] = np.ones(npts[i] + 1) * _dlogm

                # compute the other dimensions
                newt[a:b]  = np.linspace(logT[i], logT[i + 1], npts[i] + 1, endpoint=False)
                newg[a:b]  = np.linspace(logg[i], logg[i + 1], npts[i] + 1, endpoint=False)
                newl[a:b]  = np.linspace(logL[i], logL[i + 1], npts[i] + 1, endpoint=False)
            else:  # if the maximumum allowable difference is small, then just store the good point
                newt[a]  = logT[i]
                newg[a]  = logg[i]
                newl[a]  = logL[i]
                newm[a]  = logM[i]
                newdm[a] = lMj - lMi
        # tack on the last point on the grid, as the loop is one element short
        # however, we need to shift the values by 0.5 dlog(x). It comes:
        newdm = np.diff(newm)
        bins = newm[:]
        newm = newm[:-1] + 0.5 * np.diff(newm)
        newt = newt[:-1] + 0.5 * np.diff(newt)
        newg = newg[:-1] + 0.5 * np.diff(newg)
        newl = newl[:-1] + 0.5 * np.diff(newl)

        table = Table( {'logM':newm,
                        'logT':newt,
                        'logg':newg,
                        'logL':newl,
                        'dlogm':newdm, 
                        'Z': np.ones(len(newm)) * Z[0]} )

        for k in iso.header.keys():
            table.header[k] = iso.header[k]

        # keep parameters in the header of the table
        table.header['NAME'] = 'Continuously resampled ' + table.header['NAME']
        table.header['dlogT'] = dlogt
        table.header['dlogM'] = dlogm
        table.header['dlogL'] = dlogl

        if kwargs.get('ret_bins', None) is not None:
            return table, bins
        return table

    # Backward API compatibility
    _get_continuous_isochrone = get_continuous_isochrone
    _get_isochrone = get_isochrone
