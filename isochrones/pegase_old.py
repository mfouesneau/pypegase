"""
Pegase Isochrones
"""
from __future__ import print_function
import numpy as np
import tables
from numpy import interp

from ..units import unit, hasUnit
from ..io import Table
from ..config import localpath
from .isochrone import Isochrone


class pegase_old(Isochrone):
    """ Old HDF5 format interface """
    def __init__(self):
        self.name   = 'Pegase.2 (Fioc+1997)'
        self.source = localpath + '/libs/pegase.iso.hd5'
        self.data   = tables.openFile(self.source)
        self.ages   = np.sort(np.asarray([k.attrs.time for k in self.data.root.Z02]) * 1e6)
        self.Z      = np.asarray([ float('0.' + k[1:]) for k in self.data.root._g_listGroup(self.data.getNode('/'))[0]])

    def __getstate__(self):
        self.data.close()
        self.data = None
        return self.__dict__

    def __setstate__(self, d):
        self.__dict__ = d
        self.data = tables.openFile(self.source)

    def __del__(self):
        if self.data is not None:
            self.data.close()

    def _get_isochrone(self, age, metal=None, FeH=None, inputUnit=unit['yr'],
                       masses=None, *args, **kwargs):
        """ Retrieve isochrone from the original source
            internal use to adapt any library
        """

        if hasUnit(age):
            _age = int(age.to('yr').magnitude)
            _age_Myr = int(age.to('Myr').magnitude)
        else:
            _age = int(age * inputUnit.to('yr').magnitude)
            _age_Myr = int(age * inputUnit.to('Myr').magnitude)

        assert ((metal is not None) | (FeH is not None)), "Need a chemical par. value."

        if (metal is not None) & (FeH is not None):
            print("Warning: both Z & [Fe/H] provided, ignoring [Fe/H].")

        if metal is None:
            metal = self.FeHtometal(FeH)

        assert (metal in self.Z), "Metal %f not find in %s" % (metal, self.Z)
        # node = self.data.getNode('/Z' + str(metal)[2:])

        data = {}
        if _age in self.ages:
            # no interpolation, isochrone already in the file
            t = self.data.getNode('/Z' + str(metal)[2:] + '/a' + str(_age_Myr))
            # make sure you have ordered masses (by def of isochrone)
            s = np.argsort(t.col('logM')[:])

            for kn in t.colnames:
                data[kn] = t.col(kn)[s]
        else:
            # interpolate between isochrones
            d      = (self.ages - float(_age)) ** 2
            a1, a2 = np.sort(self.ages[np.argsort(d)[:2]] * 1e-6)
            print("Warning: Interpolation between %d and %d Myr" % (a1, a2))
            r = np.log10(_age / a1) / np.log10(a2 / a1)

            t1 = self.data.getNode('/Z' + str(metal)[2:] + '/a' + str(int(a1)))
            t2 = self.data.getNode('/Z' + str(metal)[2:] + '/a' + str(int(a2)))

            stop = min(t1.nrows, t2.nrows)

            # make sure you have ordered masses (by def of isochrone)
            s1 = np.argsort(t1.col('logM')[:])
            s2 = np.argsort(t2.col('logM')[:])

            for kn in t1.colnames:
                y2 = t2.col(kn)[s1][:stop]
                y1 = t1.col(kn)[s2][:stop]
                data[kn] = y2 * r + y1 * (1. - r)
                del y1, y2

        # mass selection
        if masses is not None:
            # masses are expected in logM for interpolation
            if masses.max() > 2.3:
                _m = np.log10(masses)
            else:
                _m = masses
            data_logM = data['logM'][:]
            for kn in data:
                data[kn] = interp(_m, data_logM, data[kn])

        table = Table(data, name='Isochrone from %s' % self.name)
        table.header['metal'] = metal
        table.header['time'] = _age * 1e6
        return table


class pegase(Isochrone):
    def __init__(self):
        self.name   = 'Pegase.2 (Fioc+1997)'
        self.source = localpath + '/libs/pegase.iso.fits'
        self.data   = Table(self.source)
        self.ages   = np.unique(self.data['age'])
        self.Z      = np.unique(self.data['Z'])

    def _get_isochrone(self, age, metal=None, FeH=None, inputUnit=unit['yr'], masses=None, *args, **kwargs):
        """ Retrieve isochrone from the original source
            internal use to adapt any library
        """

        if hasUnit(age):
            _age = int(age.to('yr').magnitude)
        else:
            _age = int(age * inputUnit.to('yr').magnitude)

        assert ((metal is not None) | (FeH is not None)), "Need a chemical par. value."

        if (metal is not None) & (FeH is not None):
            print("Warning: both Z & [Fe/H] provided, ignoring [Fe/H].")

        if metal is None:
            metal = self.FeHtometal(FeH)

        assert (metal in self.Z), "Metal %f not find in %s" % (metal, self.Z)
        # node = self.data.getNode('/Z' + str(metal)[2:])

        if _age in self.ages:
            # no interpolation, isochrone already in the file
            data = self.data.selectWhere('*', '(Z == {metal:f}) & (age == {age:f})'.format(age=_age, metal=metal))
            # make sure you have ordered masses (by def of isochrone)
            data.sort('logM')
        else:
            # interpolate between isochrones
            d = (self.ages - float(_age)) ** 2
            a1, a2 = np.sort(self.ages[np.argsort(d)[:2]])

            print("Warning: Interpolation between %d and %d Myr" % (a1, a2))

            r = np.log10(_age / a1) / np.log10(a2 / a1)

            t1 = self.data.selectWhere('*', '(Z == {metal:f}) & (age == {age:f})'.format(age=a1, metal=metal))
            t2 = self.data.selectWhere('*', '(Z == {metal:f}) & (age == {age:f})'.format(age=a2, metal=metal))
            stop = min(t1.nrows, t2.nrows)

            # make sure you have ordered masses (by def of isochrone)
            t1.sort('logM')
            t2.sort('logM')

            data = {}
            for kn in t1.keys():
                y2 = t2[kn][:stop]
                y1 = t1[kn][:stop]
                data[kn] = y2 * r + y1 * (1. - r)
                del y1, y2
            data = Table(data)

        # mass selection
        if masses is not None:
            # masses are expected in logM for interpolation
            if masses.max() > 2.3:
                _m = np.log10(masses)
            else:
                _m = masses
            data_logM = data['logM']
            _data = {}
            for kn in data:
                _data[kn] = interp(_m, data_logM, data[kn])

            data = Table(data)

        data.header['NAME'] = 'Isochrone from %s' % self.name
        data.header['metal'] = metal
        data.header['time'] = _age
        return data
