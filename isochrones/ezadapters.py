""" Class Adapter definitions for external isochrones """
from __future__ import print_function
import numpy as np
from numpy import interp

from .isochrone import Isochrone
from ..helpers import val_in_unit
from ..io import Table

# Use internal version
from .ezpadova import parsec
from .ezmist import ezmist
from .ezdart import ezdart



class Parsec(Isochrone):
    """ Padova-Trieste Isochrones 

    This small package provides a direct interface to the PADOVA/PARSEC
    isochrone webpage (http://stev.oapd.inaf.it/cgi-bin/cmd). It compiles the
    URL needed to query the website and retrives the data into a python
    variable.

    Interface to ezpadova (https://github.com/mfouesneau/ezpadova)
    """
    def __init__(self):
        self.name   = 'Parsec Isochrones'

    def get_isochrone(self, age, metal=None, FeH=None, masses=None, *args,
                      **kwargs):
        """ Generate an isochrone from the library

        Parameters
        ----------
        age: float, optional units
            age of the sequence. Assuming age in years if not associated with
            units.

        metal: float
            metallicity Z (e.g., Zsun = 0.02)

        FeH: float
            Chemical abundance normalized to solar abundance
            (e.g., [Fe/H]_sun = -4.33 )

        masses: ndarray, float, optional
            masses at which resample the isochrone

        .. note::

            do not provide both metal and FeH. Only metal will be used.

            No interpolation in metallicity yet
        """
        _age = val_in_unit('age', age, 'yr').magnitude

        if ((metal is None) & (FeH is None)):
            raise ValueError("Need a chemical parameter value (metal or FeH")

        if (metal is not None) & (FeH is not None):
            print("Warning: both Z & [Fe/H] provided, ignoring [Fe/H].")

        if metal is None:
            metal = self.FeHtometal(FeH)
            
        data = parsec.get_one_isochrone(_age, metal, **kwargs)

        # mass selection
        if masses is not None:
            # masses are expected in logM for interpolation
            _m = np.log10(val_in_unit('masses', masses, 'Msun').magnitude)
            data_logM = data['logM']
            _data = {}
            for kn in data.keys():
                _data[kn] = interp(_m, data_logM, data[kn])

            data = Table(_data, header=data.header)

        data.header['NAME'] = 'Isochrone from %s' % self.name
        data.header['metal'] = metal
        data.header['time'] = _age
        return data


class MIST(Isochrone):
    """ MESA/MIST Isochrones 

    This small package provides a direct interface to the mesa/mist isochrone
    webpage. It compiles the URL needed to query the website and retrives the
    data into a python variable.

    Interface to ezpadova (https://github.com/mfouesneau/ezmist)
    """
    def __init__(self):
        self.name   = 'MESA/MIST Isochrones'

    def get_isochrone(self, age, metal=None, FeH=None, masses=None, *args,
                      **kwargs):
        """ Generate an isochrone from the library

        Parameters
        ----------
        age: float, optional units
            age of the sequence. Assuming age in years if not associated with
            units.

        metal: float
            metallicity Z (e.g., Zsun = 0.02)

        FeH: float
            Chemical abundance normalized to solar abundance
            (e.g., [Fe/H]_sun = -4.33 )

        masses: ndarray, float, optional
            masses at which resample the isochrone

        .. note::

            do not provide both metal and FeH. Only metal will be used.

            No interpolation in metallicity yet
        """
        _age = val_in_unit('age', age, 'yr').magnitude

        if ((metal is None) & (FeH is None)):
            raise ValueError("Need a chemical parameter value (metal or FeH")

        if (metal is not None) & (FeH is not None):
            print("Warning: both Z & [Fe/H] provided, ignoring [Fe/H].")

        if metal is None:
            metal = self.FeHtometal(FeH)
        if FeH is None:
            FeH = self.metalToFeH(metal)
            
        data = ezmist.get_one_isochrone(_age, metal, age_scale='linear', **kwargs)
        
        data.add_column('Z', np.ones(len(data)) * metal)

        # mass selection
        if masses is not None:
            # masses are expected in logM for interpolation
            _m = np.log10(val_in_unit('masses', masses, 'Msun').magnitude)
            data_logM = data['logM']
            _data = {}
            for kn in data.keys():
                _data[kn] = interp(_m, data_logM, data[kn])

            data = Table(_data, header=data.header)

        data.header['NAME'] = 'Isochrone from %s' % self.name
        data.header['metal'] = metal
        data.header['FeH'] = FeH
        data.header['time'] = _age
        return data


class Dartmouth(Isochrone):
    """ Dartmouth Isochrones 

    This small package provides a direct interface to the Dartmouth isochrone
    webpage. It compiles the URL needed to query the website and retrives the
    data into a python variable.

    Interface to ezpadova (https://github.com/mfouesneau/ezdart)
    """
    def __init__(self):
        self.name   = 'Dartmouth Isochrones'

    def get_isochrone(self, age, metal=None, FeH=None, masses=None, *args,
                      **kwargs):
        """ Generate an isochrone from the library

        Parameters
        ----------
        age: float, optional units
            age of the sequence. Assuming age in years if not associated with
            units.

        metal: float
            metallicity Z (e.g., Zsun = 0.02)

        FeH: float
            Chemical abundance normalized to solar abundance
            (e.g., [Fe/H]_sun = -4.33 )

        masses: ndarray, float, optional
            masses at which resample the isochrone

        .. note::

            do not provide both metal and FeH. Only metal will be used.

            No interpolation in metallicity yet
        """
        _age = val_in_unit('age', age, 'yr').to('Gyr').magnitude

        if ((metal is None) & (FeH is None)):
            raise ValueError("Need a chemical parameter value (metal or FeH")

        if (metal is not None) & (FeH is not None):
            print("Warning: both Z & [Fe/H] provided, ignoring [Fe/H].")

        if metal is None:
            metal = self.FeHtometal(FeH)
        if FeH is None:
            FeH = self.metalToFeH(metal)
            
        data = ezdart.get_one_isochrone(_age, metal, age_scale='linear', **kwargs)
        
        data.add_column('Z', np.ones(len(data)) * metal)
        data.add_column('logM', data('log10(mass)'))

        # mass selection
        if masses is not None:
            # masses are expected in logM for interpolation
            _m = np.log10(val_in_unit('masses', masses, 'Msun').magnitude)
            data_logM = data['logM']
            _data = {}
            for kn in data.keys():
                _data[kn] = interp(_m, data_logM, data[kn])

            data = Table(_data, header=data.header)

        data.header['NAME'] = 'Isochrone from %s' % self.name
        data.header['metal'] = metal
        data.header['FeH'] = FeH
        data.header['time'] = _age
        return data
