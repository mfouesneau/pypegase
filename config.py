""" Configuration file

Allow the users to update default behaviors of the code
"""
from __future__ import print_function
import os
import inspect
# Set to use some C code instead of pure python to speed up the computations.
# If False, only numpy and python code are used.
__WITH_C_LIBS__ = False
# __WITH_C_LIBS__ = True

# Default number of threads to use
__NTHREADS__ = 1

try:
    import numexpr
    numexpr.set_num_threads(__NTHREADS__)
    numexpr.set_vml_num_threads(__NTHREADS__)
except ImportError:
    pass


# Some default files
localpath = '/'.join(os.path.abspath(inspect.getfile(inspect.currentframe())).split('/')[:-1])

__default_filters_lib__ = localpath + '/libs/filters.hd5'
__default_vega__        = localpath + '/libs/vega.hd5'


def printConfig():
    print("""
    ============ pyPEGASE defaut configuration ===========
    * Including C-code during computations: {0:s}
    * Parallel processing using {1:s} threads """.format(__WITH_C_LIBS__, __NTHREADS__))


# directories
__ROOT__ = '/'.join(os.path.abspath(inspect.getfile(inspect.currentframe())).split('/')[:-1])
libsdir = __ROOT__ + '/libs/'

# Online libraries
# will be replaced by a more flexible support (JSON is easy!)
libs_server = 'http://chex.astro.washington.edu:7777/morgan/pypegaselibs/'
libs = dict(
    vega='vega.hd5',
    filters='filters.hd5',
    basel22='stellib_BaSeL_v2.2.grid.fits',
    rauch='stellib_Rauch.grid.fits',
    elodie31='stellib_ELODIE_3.1.fits',
    pegH1cont='pegase.HII.cont.fits',
    pegH1line='pegase.HII.lines.fits',
    pegisoch='pegase.iso.fits',
    padova10='padova2010.iso.fits',
)
