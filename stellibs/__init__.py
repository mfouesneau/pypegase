""" 
This is a set of tools to compute synthetic spectra from libraries in a
simple way, ideal to integrate in larger projects.

In this package, multiple spectral libraries and atmosphere libraries are
provided with a common usage. This allows any user to transparently switch from
one set of models to another without pain (or combine sets together)

A standalone version is available  http://mfouesneau.github.io/docs/pystellibs/
"""
from .stellib import Stellib, CompositeStellib, AtmosphereLib
from . import interpolator
from .basel import BaSeL
from .elodie import Elodie
from .rauch import Rauch
from .tlusty import Tlusty
from .kurucz import Kurucz
from ..io.simpletable import SimpleTable
from ..units import unit
