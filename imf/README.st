(Stellar) Initial Mass Function
===============================

the initial mass function (IMF) is an empirical function that describes the
distribution of initial masses for a population of stars. 

We implemented the IMF as a probability distribution function (PDF) for the mass
at which a star enters the main sequence evolution (ZAMS).


