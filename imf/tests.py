from . import common

import pylab as plt
import numpy as np
from scipy import special


def plot_imf_law(l, ax=None):
    """ plot the representation of the distribution """

    m = np.hstack((l.massinf[:-1], [min(l.massinf[-1], l.massMax)]))
    v = l(m)

    ax = ax or plt.gca()
    ax.loglog(m, v, label=l.name)


def plot_all_definitions():
    ax = plt.subplot(111)
    for cl in common.__all__:
        plot_imf_law(eval('common.{0:s}()'.format(cl)), ax=ax)

    ax.set_xlabel('Stellar Mass [M/M$_\odot$]')
    ax.set_ylabel('P(M | IMF) dM')
    plt.legend()


def ppf_poisson(mu, q):
    vals = np.ceil(special.pdtrik(q, mu))
    vals1 = np.maximum(vals - 1, 0)
    temp = special.pdtr(vals1, mu)
    return np.where(temp >= q, vals1, vals)


def plot_rvs(l, N=1e4, mc=10, dlogm=0.2, ax=None):

    ax = ax or plt.gca()
    logb = np.arange(np.log10(l.massinf[0]), np.log10(l.massinf[-1]), dlogm)
    #logbc = 0.5 * (logb[:-1] + logb[1:])
    b = 10 ** logb
    #bc = 10 ** logbc
    bc = 0.5 * (10 ** logb[1:] + 10 ** logb[:-1])
    lbl = 'Samples'
    for mck in range(mc):
        r = l.rvs(N, b.min(), b.max())
        n, _ = np.histogram(np.log10(r), bins=logb)
        n = n.astype(float) * 10 ** dlogm / bc
        #ax.step(bc, n, where='mid', color='0.0', alpha=0.2, lw=0.5)
        ax.plot(bc + np.random.uniform(-0.45, 0.45, len(bc)) * np.diff(b),
                n + np.random.uniform(-0.5, 0.5, len(n)),
                'o', color='0.0', alpha=0.2, lw=0.5, label=lbl, ms=3)
        lbl = None

    #plot prediction and Poisson variations around the predicted mean
    #---------------------------------------------------------------
    #
    # `nexpected` is computed from the enclosed fractional of stars in a given
    # mass range from the IMF object. This number needs to be rescaled by the
    # total number of stars N.
    # In addition the bins we used above are uniform in log-mass but not in
    # mass. Thus we need to correct for non uniform bin sizes
    #
    # finally, the number of stars from the IMF is the predicted mean value of
    # the bin on which Poisson variations are expected.
    ne = N * np.array([l.get_enclosed_Nstar(b[k], b[k + 1]) for k in range(len(b) - 1)])

    # proper Poisson percentiles (because of log-binning that is not perfect)
    p01 = np.array([ ppf_poisson(nek, 0.01) for nek in ne ]) * 10 ** dlogm / bc
    p16 = np.array([ ppf_poisson(nek, 0.16) for nek in ne ]) * 10 ** dlogm / bc
    p50 = np.array([ ppf_poisson(nek, 0.50) for nek in ne ]) * 10 ** dlogm / bc
    p84 = np.array([ ppf_poisson(nek, 0.84) for nek in ne ]) * 10 ** dlogm / bc
    p99 = np.array([ ppf_poisson(nek, 0.99) for nek in ne ]) * 10 ** dlogm / bc

    # median_approx = np.floor(ne + 1. / 3. - 0.02 * ne)
    # plt.plot(bc, median_approx * 10 ** dlogm / bc, 'ro')

    ne = ne * 10 ** dlogm / bc
    plt.loglog(bc, ne, 'r-', ls='-', lw=2, label='Prediction')

    # plot 1sigma boxes
    plt.step(b[:-1], p01, color='y', lw=2, where='post', label='99\% Poisson')
    plt.step(b[:-1], p99, color='y', lw=2, where='post')
    plt.step(b[:-1], p16, color='#aa3300', lw=2, where='post', label='68\% Poisson')
    plt.step(b[:-1], p84, color='#aa3300', lw=2, where='post')
    plt.step(b[:-1], p50, color='0.80', lw=1, where='post', label='50\% Poisson')

    #make crosses instead of boxes
    #lbl = 'IMF + 68\% Poisson'
    #for k in range(len(bc)):
        #plt.plot([b[k], bc[k], b[k + 1]], [p01[k]] * 3, 'b-', lw=1, ms=10, mew=2, zorder=-10, label=lbl)
        #plt.plot([b[k], bc[k], b[k + 1]], [p99[k]] * 3, 'b-', lw=1, ms=10, mew=2, zorder=-10, label=lbl)
        #plt.plot([b[k], bc[k], b[k + 1]], [p16[k]] * 3, 'b-', lw=1, ms=10, mew=2, zorder=-10, label=lbl)
        #plt.plot([b[k], bc[k], b[k + 1]], [p84[k]] * 3, 'b-', lw=1, ms=10, mew=2, zorder=-10)
        #plt.plot([b[k]] * 3, [p16[k], p50[k], p84[k]], 'b-', lw=1, ms=10, mew=2, zorder=-10)
        #plt.plot([b[k + 1]] * 3, [p16[k], p50[k], p84[k]], 'b-', lw=1, ms=10, mew=2, zorder=-10)
        #plt.plot([bc[k]], [p50[k]], 'b+', lw=2, ms=10, mew=2, zorder=-10)
        #lbl = None

    ax.set_xscale('log')
    ax.set_yscale('log')
    #ax.set_ylim(0.1, ax.get_ylim()[1])
    ax.set_xlabel('Stellar Mass [M/M$_\odot$]')
    ax.set_ylabel('dN/dM')

    plt.legend(frameon=False, markerscale=3, numpoints=1)
