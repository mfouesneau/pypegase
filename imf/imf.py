"""
(Initial) MASS FUNCTION CLASSES
===============================

This class implement a mass function object defined from multiple intervals and
slopes. It computes the full function incl. continuity and normalization.  The
resulting object can be used as a python function

Some common IMF are already implemented in this module KENNICUTT, KROUPA93,
SALPETER ...


This python file implements *one* solution to the IMF class creation and using
inheritance to define multiple functions from the literature.
implementation considers the IMF as :math:`dN / dM`, in which Salpeter is -2.35
(in opposition to :math:`dN / dlog(m)`)


.. note::

    the implementation is of course not unique.


:author: Morgan Fouesneau
:email: fouesneau@mpia.de
"""
import numpy as np

__all__ = ['IMF']


class IMF(object):
    """
    Here is one possible implementation, which
    let you define an IMF as multiple power-laws

    Attributes
    ----------
    nI: int
        number of definition intervals

    x:  iterable
        interval edges (of len nI + 1)

    a:  iterable
        power-law indexes (of len nI) in units of dN/dM (Salpeter corresponds to -2.35)

    massMin: float
        minimal mass

    massMax:
        maximal mass

    name: string
        optional name

    .. notes::
        1 - the mass range can be restricted by massMin, massMax while
            keeping the official definition.
        2 - indexes of the different power-laws are assumed to be in units
            of dN/dM, ie, -2.35 corresponds to a Salpeter IMF. However,
            sometimes you could find -1.35, which corresponds to an index
            defined in terms of dN/dlog(M).
    """
    def __init__(self, nI, x, a, massMin=0.1, massMax=120., name=None):
        """__init__ - constructor of the class

        Parameters
        ----------
        nI: int
            number of definition intervals

        x:  iterable
            interval edges (of len nI + 1)

        a:  iterable
            power-law indexes (of len nI) in units of dN/dM (Salpeter corresponds to -2.35)

        massMin: float
            minimal mass

        massMax:
            maximal mass

        name: string
            optional name

        .. notes::
            1 - the mass range can be restricted by massMin, massMax while
                keeping the official definition.
            2 - indexes of the different power-laws are assumed to be in units
                of dN/dM, ie, -2.35 corresponds to a Salpeter IMF. However,
                sometimes you could find -1.35, which corresponds to an index
                defined in terms of dN/dlog(M).
        """
        # Store relevant information
        # ==========================
        self.nIMFbins  = nI
        self.massinf   = np.asarray(x)
        self.slope     = np.asarray(a)
        self.name      = name
        self.coeffcont = np.zeros(np.size(a))
        self.massMin   = massMin
        self.massMax   = massMax

        # Make functional form
        # ====================
        # the first step is to build the functional form of the IMF:
        # i.e., make a continuous function and compute the normalization

        # continuity
        # ----------
        # given by:  c[i-1] * x[i] ^ a[i-1] = c[i] * x[i] ^ a[i]
        # arbitrary choice for the first point, which will be corrected by the
        # normalization step.
        self.coeffcont[0] = 1.
        for i in range(1, nI):
            self.coeffcont[i]  = (self.coeffcont[i - 1])
            self.coeffcont[i] *= (x[i] ** (a[i - 1] - a[i]))

        # normalize
        # ---------
        # depends on the adpoted definition of the IMF indexes:
        # either dN/dM or dN/dlog(M). In this example we consider that indexes
        # are given in units of dN/dM.
        # get the norm : integral(IMF(M) dM) = 1 seen as a prob dist. funct."""
        self.norm = 1.  # will be updated at the next line
        self.norm = self.get_enclosed_Nstar(self.massMin, self.massMax)

        self.norm_M = self.get_enclosed_mass(self.massMin, self.massMax)

        # Compute the average mass
        self.avg = self.get_avg_mass(self.massMin, self.massMax)

    def cdf(self, mass, massMin=None, massMax=None):
        """ Cumulative distribution

        .. math:: \\int_{Mmin}^{Mmax} IMF(m) dm = \\int_{Mmin}^{Mmax} \\frac{dN}{dm} dm

        Parameters
        ----------
        mass: float or sequence
            masses at which evaluate the cumulative distribution
            in Msun

        massMin: float, optional
            set the mass range lower limit of the normalization
            in Msun
            (default is to use the IMF definition)

        massMax: float, optional
            set the mass range upper limit of the normalization
            in Msun
            (default is to use the IMF definition)

        Returns
        -------
        values: float or ndarray
            return the evaluation values as float or array of floats
        """

        massMin = massMin or self.massMin
        massMax = massMax or self.massMax

        if (massMin >= self.massMax) | (massMax <= self.massMin):
            raise ValueError('massMin/massMax do not overlap with the definition range')

        #make a vectorial function
        _mass = np.atleast_1d(mass).astype(float)

        values = np.zeros(_mass.shape, _mass.dtype)
        values[_mass > massMax] = 1.
        #_mass < massMin already set to return 0

        _norm = self.get_enclosed_Nstar(massMin, massMax)
        for k in range(len(_mass)):
            if massMax > _mass[k] > massMin:
                values[k] = self.get_enclosed_Nstar(massMin, _mass[k]) / _norm

        if hasattr(mass, '__iter__'):
            return values
        else:
            return float(values)

    def get_enclosed_Nstar(self, Mmin, Mmax):
        """get_enclosed_Nstar - Get the enclosed dN over a given mass range.

        .. math::

            \\int_{Mmin}^{Mmax} IMF(m) dm = \\int_{Mmin}^{Mmax} \\frac{dN}{dm} dm

        .. note::

            no extrapolation outside the original mass range definition of the IMF.

        Parameters
        ----------
        Mmin: float
            lower limit of the mass range
            in Msun

        Mmax: float
            upper limit of the mass range
            in Msun

        returns
        -------
        r: float
            enclosed dN within [Mmin, Mmax]
        """
        x = self.massinf
        a = self.slope
        c = self.coeffcont

        # will be useful in the integration
        b = a + 1.

        val = 0.
        # analytical integration of a power law
        for i in range(0, self.nIMFbins):
            if (Mmin < x[i + 1]) & (Mmax > x[i]):
                if x[i] <= Mmin:
                    x0 = Mmin
                else:
                    x0 = x[i]
                if x[i + 1] <= Mmax:
                    x1 = x[i + 1]
                else:
                    x1 = Mmax
                # careful if the index is -1
                if abs(b[i]) < 1e-30:
                    S = c[i] * (np.log(x1) - np.log(x0))
                else:
                    S = c[i] / b[i] * ( (x1) ** (b[i]) - (x0) ** (b[i]) )
                val += S

        return val  / self.norm

    def nstars(self, logm, dlogm=None, edges=None, logm_min=None, logm_max=None, ret_bins=False):
        """
        Computes the expected number of stars in [`logm`, `logm +
        dlogm`] intervals, normalized to a total mass of M = 1 Msun, for a given list
        of masses and mass bins.

        If `dlogm` is not provided, it assumes m to be the centers of mass bins
        (must be sorted values) If `dlogm` is not provided, bin edges are
        defined as the midpoints between subsequent `logm`.

        If `edges` are provided, they take precendence over any dlogm that
        might have also been given. `len(edges)` should be `len(logm)+1`, and the
        `edges` should bracket the `logm`.
        If `edges` are provided, `logm` is not actually used in any manner, but
        logm consistency is checked (each `edges[i] <= logm[i] <= edges[i+1]`).


        The mass intervals are given in log10 units and, if needed, `dlogm`.

        .. math::

            N = \\int_{Mmin}^{Mmax} IMF(m) dm = \\int_{Mmin}^{Mmax} \\frac{dN}{dm} m d\\log m

        .. note::

            the default mass range is the smallest between the optional range
            given by logm_min, logm_max  and the mass function definition range

        Parameters
        ----------
        logm: ndarray (dtype=float, ndim=1)
            list of log(masses)
            list must be sorted in increasing order

        dlogm: ndarray, optional (dtype=float, ndim=1)
            list of dlog(masses)

        ret_bins: bool
            if set returns the bins

        Returns
        -------
        dN:  ndarray[float, ndim=1]
            number of stars per mass bin for a population of unit total mass
        """
        n_masses = len(logm)
        vals = np.zeros(n_masses, dtype=float)

        if edges is None:

            # book keeping, only when requested
            if ret_bins:
                edges = []

            if dlogm is None:
                # values are defined by central points between 2 successive bins
                #
                # |-- ... -*--|--*---|----*---|-----*-- ... ---|
                # 0           k     k+1      k+2              n-1
                #

                #first value, assumed center of a symmetric bin (in log)
                m0 = 1.5 * logm[0] - 0.5 * logm[1]
                m1 = 0.5 * (logm[1] + logm[0])
                vals[0] = self.get_enclosed_Nstar(10 ** m0, 10 ** m1)

                if ret_bins:
                    edges.append((m0, m1))

                # If a bin has no length this sucks! (but still works)
                for k in range(n_masses - 2):
                    m0 = 0.5 * (logm[k + 1] + logm[k])
                    m1 = 0.5 * (logm[k + 2] + logm[k + 1])
                    vals[k + 1] = self.get_enclosed_Nstar(10 ** m0, 10 ** m1)
                    if ret_bins:
                        edges.append((10 ** m0, 10 ** m1))

                #last value
                m0 = 0.5 * (logm[-1] + logm[-2])
                m1 = 1.5 * logm[-1] - 0.5 * logm[-2]
                vals[-1] = self.get_enclosed_Nstar(m0, m1)
                if ret_bins:
                    edges.append((m0, m1))
            else:
                for k in range(n_masses):
                    m0 = logm[k + 1] - 0.5 * dlogm[k]
                    m1 = logm[k + 1] + 0.5 * dlogm[k]
                    vals[k] = self.get_enclosed_Nstar(10 ** m0, 10 ** m1)
                    if ret_bins:
                        edges.append((m0, m1))
        else:
            # edges assumed in log(Mass)
            for k in range(n_masses):
                if not (edges[k] <= logm[k] <= edges[k+1]):
                    raise ValueError("an error or at least a warning")
                vals[k] = self.get_enclosed_Nstar(10 ** (edges[k]), 10 ** edges[k + 1])

        return vals / self.norm_M   #  strictly dn/ntot / (mtot/ntot)

    def nstars1(self, logm):
        """ created for debugging nstars """

        n_masses = len(logm)
        vals = np.zeros(n_masses, dtype=float)

        dlogm = np.diff(logm)
        vals[1:] = [self.get_enclosed_Nstar(10 ** (lm - 0.5 * dlm), 
                                            10 ** (lm + 0.5 * dlm)) 
                    for lm, dlm in zip(logm[1:], dlogm)] 
        vals[0] = self.get_enclosed_Nstar(10 ** (logm[0] - 0.5 * dlogm[0]),
                                          10 ** (logm[0] + 0.5 * dlogm[0]))
        vals /= self.norm_M
        return vals

    def get_enclosed_mass(self, Mmin, Mmax):
        """Get the enclosed mass over a given mass range.

        .. math::

            M = \\int_{Mmin}^{Mmax} IMF(m) m dm = \\int_{Mmin}^{Mmax} \\frac{dN}{dm} m dm

        .. note::

            no extrapolation outside the original mass range definition of the IMF.

        Parameters
        ----------
        Mmin: float
            lower mass range

        Mmax: float
            upper mass range

        returns
        -------
        r: float
            enclosed mass within [Mmin, Mmax]
        """
        x = self.massinf
        a = self.slope
        c = self.coeffcont

        # will be useful in the integration
        b = a + 2.

        val = 0.
        # analytical integration of a power law
        for i in range(0, self.nIMFbins):
            if (Mmin < x[i + 1]) & (Mmax > x[i]):
                if x[i] <= Mmin:
                    x0 = Mmin
                else:
                    x0 = x[i]
                if x[i + 1] <= Mmax:
                    x1 = x[i + 1]
                else:
                    x1 = Mmax

                # careful if the index is 1
                if abs(b[i]) < 1e-30:
                    S = c[i] * (np.log(x1) - np.log(x0))
                else:
                    S = c[i] / b[i] * ( (x1) ** (b[i]) - (x0) ** (b[i]) )
                val += S

        return val / self.norm

    def getValue(self, m):
        """
        returns the value of the normalized IMF at a given mass m

        .. math::

            IMF(m) = \\frac{c}{norm} m ^ {\\alpha}

            norm \\times \\int_{Mmin}^{Mmax} IMF(m) dm = 1

        Parameters
        ----------
        m: float or iterable of floats
            masses at which evaluate the function

        returns
        -------
        r: float or ndarray(dtype=float)
            evaluation of the function (normalized imf)
        """
        # if m is iterable
        if getattr(m, '__iter__', False):
            return np.asarray([ self.getValue(mk) for mk in m])
        else:
            # extrapolation
            if (m > self.massMax) or (m < self.massMin):
                return 0.
            # exact value exists
            elif m in self.massinf[:-1]:
                ind = np.where(m == self.massinf)
                return float(float(self.coeffcont[ind]) / self.norm_M * m ** self.slope[ind])
            # otherwise do the evaluation with the correct interval
            else:
                i = 0
                if self.nIMFbins > 1:
                    while m > self.massinf[i]:
                        i += 1
                    i -= 1
                if self.massinf[i] > self.massMax:
                    return 0.
                else:
                    return float(float(self.coeffcont[i]) / self.norm * m ** self.slope[i])

    def get_avg_mass(self, Mmin, Mmax):
        """ get the average mass over a given range

        .. math::

            \\langle M \\rangle  = \\frac{\\int_{Mmin}^{Mmax}{IMF(m) m dm}}{\\int_{Mmin}^{Mmax}{IMF(m) dm}}

        Parameters
        ----------
        Mmin: float
            lower mass range
            in Msun

        Mmax: float
            upper mass range
            in Msun

        Returns
        -------
        M: float
            average mass in [Mmin, Mmax]

        """
        return self.get_enclosed_mass(Mmin, Mmax) / self.get_enclosed_Nstar(Mmin, Mmax)

    def random(self, N, massMin=None, massMax=None):
        """random - Draw mass samples from this distribution
        Samples are distributed over the interval [massMin, massMax] Interval
        is truncated to the IMF range definition if it extents beyond it.
        (taken as is otherwise)

        Parameters
        ----------
        N: int
            size of the sample

        massMin: float
            lower mass (default self.massMin)

        massMax: float
            upper mass (default self.massMax)

        returns
        -------

        r: ndarray(dtype=float)
            returns an array of random masses


        method
        ------

        drawing random numbers from a given distribution can be done using
        multiple methods. When you can compute or accurately estimate the
        integral of your function, the optimal way is to use it, aka
        "Inverse transform sampling". Briefly:

        let's call :math:`F(x) = \\int_{Mmin}^x IMF(m) dm`

        Since we use power laws, the intregral is trivial. if :math:`x`
        such as: :math:`M[i] <= x < M[i+1]`, with :math:`M` the masses used
        to define the IMF:

        .. math::

            F(x) = F(M[i]) + 1 / norm \,\\times 1 / (\\alpha[i] + 1) ( x ^ {\\alpha[i] + 1} - M[i] ^ (\\alpha[i] + 1) )


        The trick is to observe that :math:`F(x)` varies between 0 and 1,
        and that there is a unique mapping between :math:`F(x)` and
        :math:`x` (:math:`F` is a bijective function).  I spare the proof
        but we can demonstrate that drawing uniform :math:`F(x)` numbers
        between 0 and 1 will then give us :math:`x` values that exactly
        follow the IMF.

        In our case, the previous equation is inversible, and once you
        extract :math:`x` as a function of :math:`F(x)` you're done.
        """

        # check keyword values, default is the IMF definition
        massMin = massMin or self.massMin
        massMax = massMax or self.massMax

        beta = self.slope + 1.

        # compute the cumulative distribution values at each mass interval edge
        F = np.zeros(self.nIMFbins + 1)
        F[-1] = 1.0
        for i in range(1, self.nIMFbins):
            F[i] = F[i - 1] + 1. / self.norm * (self.coeffcont[i - 1] / (beta[i - 1])) * ( self.massinf[i] ** (beta[i - 1]) - self.massinf[i - 1] ** (beta[i - 1]) )

        #find intervals of massMin and massMax
        for k in range(self.nIMFbins):
            if massMin >= self.massinf[k]:
                mink = k
            if massMax >= self.massinf[k]:
                maxk = k
        if massMin < self.massMin:
            Fmin = 0.
        elif massMin >= self.massMax:
            return
        else:
            i = mink
            Fmin = F[i] + 1. / self.norm * ( self.coeffcont[i] / (beta[i]) ) * ( massMin ** (beta[i]) - self.massinf[i] ** (beta[i]) )

        if massMax >= self.massMax:
            Fmax = 1.0
        elif massMax < self.massMin:
            return
        else:
            i = maxk
            Fmax = F[i] + 1. / self.norm * ( self.coeffcont[i] / (beta[i]) ) * ( massMax ** (beta[i]) - self.massinf[i] ** (beta[i]) )

        x = np.random.uniform(Fmin, Fmax, N)
        y = np.zeros(N)
        for k in range(self.nIMFbins):
            ind = np.where((x >= F[k]) & (x < F[k + 1]))
            if len((ind)[0]) > 0:
                y[ind] = self.massinf[k] * ( 1. + (x[ind] - F[k]) / (self.massinf[k] ** (beta[k])) * ( beta[k] ) * self.norm / self.coeffcont[k]) ** (1. / (beta[k]))
        return y

    def rvs(self, *args, **kwargs):
        """ see :func:`random` """
        return self.random(*args, **kwargs)

    def __call__(self, m=None):
        """__call__ - make a callable object (function like)

        Parameters
        ----------

        m: float or iterable or None
            if None calls self.info()
            else calls self.getValue(m)

        .. see also::
            :func:`getValue`, :func:`info`
        """
        if m is None:
            return self.info()
        return self.getValue(m)

    def info(self):
        """ prints a quick summary of the functional object """
        txt = """IMF: {s.name}, IMF: dN/dM = 1./norm * c * m ** a
        nI = {s.nIMFbins},
        norm = {s.norm}, 1/norm = {invnorm},
        Average mass = {s.avg} [Msun],
        m[] = {s.massinf}  [Msun],
        a[] = {s.slope},
        c[] = {s.coeffcont}"""
        print(txt.format(s=self, invnorm=1. / self.norm))
