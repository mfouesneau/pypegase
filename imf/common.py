"""
(Initial) MASS FUNCTION CLASSES
===============================

Some common IMF are already implemented in this module KENNICUTT, KROUPA93,
SALPETER ...

:author: Morgan Fouesneau
:email: mfouesn@uw.edu
"""
import numpy as np
from .imf import IMF


# Deriving common IMF from the literature
#=========================================

# in the definitions below, power-law indexes are given for the dN/dlog(M)
# definition and accordingly converted before usage

__all__ = ['Kennicutt', 'Kroupa2001', 'Kroupa93', 'MillerScalo', 'Salpeter',
           'Scalo86', 'Scalo98']


class Kennicutt(IMF):
    """ Kennicutt (1983) doi:10.1086/161261"""
    def __init__(self):
        nI = 2
        x = [0.1, 1., 120.]
        a = [-0.4, -1.5]
        a = np.asarray(a) - 1
        massMin = 0.1
        massMax = 120.
        IMF.__init__(self, nI, x, a, massMin, massMax, name='Kennicutt')


class Kroupa2001(IMF):
    """ Kroupa (2001)
    "On the variation of the initial mass function".
    MNRAS 322: 231. doi:10.1046/j.1365-8711.2001.04022.x.
    """
    def __init__(self):
        nI = 4
        x = [0.01, 0.08, 0.5, 1., 150.]
        a = [0.7, -0.3, -1.3, -1.3]
        a = np.asarray(a) - 1
        massMin = 0.01
        massMax = 150.
        IMF.__init__(self, nI, x, a, massMin, massMax, name='Kroupa 2001')


class Kroupa93(IMF):
    """ Kroupa,Tout, & Gilmore (1993)
    "The distribution of low-mass stars in the Galactic disc"
    MNRAS, 262, 545
    """
    def __init__(self):
        nI = 3
        x = [0.1, 0.5, 1., 120.]
        a = [-0.3, -1.2, -1.7]
        a = np.asarray(a) - 1
        massMin = 0.1
        massMax = 120.
        IMF.__init__(self, nI, x, a, massMin, massMax, name='Kroupa 1993')


class Salpeter(IMF):
    """
    Salpeter (1955).
    "The luminosity function and stellar evolution",
    ApJ, 121, 161. doi:10.1086/145971
    """
    def __init__(self):
        nI = 1
        x = [0.1, 120.]
        a = [-1.35]
        a = np.asarray(a) - 1
        massMin = 0.1
        massMax = 120.
        IMF.__init__(self, nI, x, a, massMin, massMax, name='Salpeter')


class MillerScalo(IMF):
    """ Miller & Scalo (1979)
    "The initial mass function and stellar birthrate in the solar neighborhood".
    ApJS 41: 513. doi:10.1086/190629.
    """
    def __init__(self):
        nI = 3
        x = [0.1, 1., 10., 120.]
        a = [-0.4, -1.5, -2.3]
        a = np.asarray(a) - 1
        massMin = 0.1
        massMax = 120.
        IMF.__init__(self, nI, x, a, massMin, massMax, name='Miller & Scalo')


class Scalo98(IMF):
    def __init__(self):
        nI = 3
        x = [0.1, 1., 10., 120.]
        a = [-0.2, -1.7, -1.3]
        a = np.asarray(a) - 1
        massMin = 0.1
        massMax = 120.
        IMF.__init__(self, nI, x, a, massMin, massMax, name='Scalo 1998')


class Scalo86(IMF):
    def __init__(self):
        nI = 24
        x = [    1.00000000e-01,   1.10000000e-01,   1.40000000e-01,
                 1.80000000e-01,   2.20000000e-01,   2.90000000e-01,
                 3.60000000e-01,   4.50000000e-01,   5.40000000e-01,
                 6.20000000e-01,   7.20000000e-01,   8.30000000e-01,
                 9.80000000e-01,   1.17000000e+00,   1.45000000e+00,
                 1.86000000e+00,   2.51000000e+00,   3.47000000e+00,
                 5.25000000e+00,   7.94000000e+00,   1.20200000e+01,
                 1.82000000e+01,   2.69200000e+01,   4.16900000e+01,
                 1.20000000e+02     ]
        a = [  3.2   ,  2.455,  2.   ,  0.3  ,  0.   ,  0.   , -0.556, -1.625,
               -1.833, -1.286,  1.5  , -1.857,  0.   , -2.333, -3.455, -1.692,
               -2.571, -1.722, -1.611, -1.667, -2.333, -1.353, -0.947, -1.778]
        a = np.asarray(a) - 1
        massMin = 0.1
        massMax = 120.
        IMF.__init__(self, nI, x, a, massMin, massMax, name='Scalo 1986')
