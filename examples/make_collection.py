"""
DEMO - How to make a collection of models

For this particular application, it becomes highly relevant to try to optimize
the memory and disk operations.

As such, in this file, we create a function based on :func:`ssp.fast_discrete`,
which will attempt to optimize computations. We do not call directly
:func:`ssp.fast_discrete` because it would make a lot of memory usage and
redundant computations.
"""
import numpy as np
from pypegase.io import Table
from pypegase.io.fits import pyfits
from pypegase.config import __NTHREADS__

from pypegase import imf
from pypegase import isochrones
from pypegase import nebular
from pypegase import stellibs
from pypegase.helpers import hasUnit
from pypegase.units import unit
from pypegase.tools.pbar import Pbar
from pypegase.ssp import fast_discrete1 as fast_discrete


def save_samples(fname, p, l, s, stars):

    def unit_drop(q, u=None):
        """ Drop units or assume its ok """
        if hasUnit(q):
            if u is not None:
                return q.to(u).magnitude
            else:
                return q.magnitude
        else:
            return q

    data = np.vstack([unit_drop(s), unit_drop(l, 'AA')])

    header = pyfits.Header()
    header['SPEC_UNIT'] = '{0:s}'.format(str(s.units))
    header['LAMB_UNIT'] = '{0:s}'.format(str(l.units))
    pyfits.writeto(fname, data, header=header)

    data = {}
    header = {}
    for k, v in p.items():
        data[k] = unit_drop(v)
        if hasUnit(v):
            header['{0:s}_UNIT'.format(k)] = str(v.units)

    Table(data, header=header).write(fname, append=True)


def main(Nstars=int(1e6), Z=0.02, fracNeb=1, nsamp=20):

    oIMF     = imf.Kroupa93()
    oISO     = isochrones.Pegase()
    oNEB     = nebular.pegase()
    #oSL      = stellibs.BaSeL()
    oSL      = stellibs.Elodie()

    for age in Pbar('Ages').iterover(oISO.ages):

        _age = age * unit['yr']
        l, s, p, stars, stars_specs, dN  = fast_discrete(Nstars, _age, Z, oIMF,
                                                         oISO, oSL, oNEB,
                                                         fracNeb=fracNeb,
                                                         dlogm=0.01,
                                                         nsamp=nsamp,
                                                         full_outputs=True,
                                                         nthreads=__NTHREADS__)

        fname = 'collection_age_{0:d}.fits'.format(int(age * 1e-6))
        save_samples(fname, p, l, s, stars)


def main_licks(Nstars=int(1e6), Z=0.02, fracNeb=1, nsamp=20):

    from pypegase import licks

    oIMF     = imf.Kroupa93()
    oISO     = isochrones.Pegase()
    oNEB     = nebular.pegase()
    #oSL      = stellibs.BaSeL()
    oSL      = stellibs.Elodie()

    _age = 5e9 * unit['yr']
    l, s, p, stars, stars_specs, dN  = fast_discrete(Nstars, _age, Z, oIMF,
                                                     oISO, oSL, oNEB,
                                                     fracNeb=fracNeb,
                                                     dlogm=0.01, nsamp=nsamp,
                                                     full_outputs=True,
                                                     nthreads=__NTHREADS__)

    # fname = 'collection_age_licks.fits'.format(int(_age.magnitude * 1e-6))

    lick = licks.read_lick_list()
    indices = {}
    lmin = l.magnitude.min()
    lmax = l.magnitude.max()
    for name, a in lick.items():
        if (a['blue'][0] >= lmin) & (a['red'][1] <= lmax):
            indices[name] = licks.get_indice(l, s,
                                             a['blue'],
                                             a['red'],
                                             a['band'],
                                             a['unit'])

    return p, l, s, stars

if __name__ == '__main__':
    # main(Nstars=int(1e3), Z=0.02, fracNeb=1, nsamp=1000)
    pass
