"""
DEMO - Example of Continuously sampled population spectral synthesis

In this example, we generate one population and build a figure representing
it's HR diagram coded by contribution and its spectral models (with and without
nebular contribution).
"""
from __future__ import print_function
import numpy as np

from pypegase.config import __NTHREADS__
from pypegase import imf
from pypegase import isochrones
from pypegase import nebular
from pypegase import stellibs
from pypegase.helpers import val_in_unit, hasUnit, unit
from pypegase.tools import figure, latexFloat, timeit
from pypegase.ssp import continuous as get_ssp


def ssp_plot(l, s, p, oIMF, oISO, oSL, oNEB):
    """
    Plot the HR diagram and the resulting spectra of the SSP generation

    .. note::
        Handles units

    Parameters
    ----------
    l: ndarray, dtype=float
        wavelength of the final spectrum
        units = 'Angstrom'

    s: ndarray, dtype=float
        final spectrum
        units = 'erg/s/AA/lsun/msun' or 'erg/s/AA/msun'

    p: dict
        properties of the population returned by `get_ssp`

    oIMF: IMF instance
        initial mass function, only used for text description

    oISO: Isochrone instance
        isochrone models, only used for text description

    oSL: Stellib instance
        Stellar library, only used for text description

    oNEB: Nebular instance
        nebular model, only used for text description

    Returns
    -------
    ax1: plt.Axes instance
        HR diagram axes

    ax2: plt.Axes instance
        Spectral panel axes

    ax3: plt.Axes instance
        Description panel
    """

    # Handle default units for the plots
    # ----------------------------------
    l = val_in_unit('wavelength', l, 'AA').magnitude
    l0 = val_in_unit('l0', p['l0'], 'AA').magnitude
    if p['inLsun']:
        s = val_in_unit('spectrum', s, 'lsun/AA/msun').magnitude
        s0 = val_in_unit('s0', p['s0'], 'lsun/AA/msun').magnitude
    else:
        s = val_in_unit('spectrum', s, 'erg/s/AA/msun').magnitude
        s0 = val_in_unit('s0', p['s0'], 'erg/s/AA/msun').magnitude
    mtot = val_in_unit('Mtot', p['Mtot'], 'msun').magnitude

    stars = p['stars']

    figure.figure(figsize=(8, 8))

    # HRdiagram: plot stellib log(g), log(Teff), selected isochrone, ...etc.
    # ----------------------------------------------------------------------
    ax1 = figure.subplot2grid((2, 2), (0, 0), 1, 1)
    # plot the grid
    ax1.plot(oSL.logT, oSL.logg, ',', color='0.0')
    # plot the isochrone
    _iso   = oISO._get_isochrone(val_in_unit('age', p['age'], 'yr'), p['Z'])
    ax1.plot(_iso['logT'], _iso['logg'])
    # plot population and used stars from the spectral library
    try:
        # assume single stellib
        Sidx = p['r'][:, 0].astype(int)
        _w   = p['r'][:, 1]
        idx = (_w > 0)
        _w[idx] = np.log10(_w[idx])
        ax1.scatter(oSL.logT[Sidx], oSL.logg[Sidx], c=_w, edgecolor='None', s=10)
    except:
        # so this mean composite stellib
        for oslk, rk in p['r']:
            Sidx = rk[:, 0].astype(int)
            _w   = rk[:, 1]
            idx = (_w > 0)
            _w[idx] = np.log10(_w[idx])
            ax1.scatter(oSL._olist[oslk - 1].logT[Sidx],
                        oSL._olist[oslk - 1].logg[Sidx], c=_w, edgecolor='None',
                        s=10)
    ax1.plot(stars['logT'], stars['logg'], '.', mec='None')
    # polish axis orientations and labels
    ax1.set_xlim(ax1.get_xlim()[::-1])
    ax1.set_ylim(ax1.get_ylim()[::-1])
    ax1.set_ylabel('log(g)')
    ax1.set_xlabel('log(Teff)')

    # Plot the final spectrum (w/ and w/o nebular component)
    # -----------------------------------------------------
    ax2 = figure.subplot2grid((2, 2), (1, 0), 1, 2)
    ax2.loglog(l0, s0)
    ax2.loglog(l, s)
    ax2.set_xlabel(r'Wavelength [$\rm{\AA}$]')
    if p['inLsun']:
        ax2.set_ylabel(r'Flux [erg/s/$\rm{\AA}$/L$_\odot$]')
    else:
        ax2.set_ylabel(r'Flux [erg/s/$\rm{\AA}$]')

    # Add text
    # --------
    try:
        ax3 = figure.subplot2grid((2, 2), (0, 1), 1, 1)
        ax3.axes.set_axis_off()
        txt  = r"""{{\bf Population characteristics}}\\
        Age: {age} Myr\\
        Z: {Z:0.4f}\\
        Nstars: {Nstars:s}\\
        Mtot: {Mtot:s} M$_\odot$"""
        txt = txt.format(age=latexFloat(val_in_unit('age', p['age'], 'Myr').magnitude),
                         Z=p['Z'], Nstars=latexFloat(p['Nstars'], '0.3'),
                         Mtot=latexFloat(float(mtot), '0.3'))
        ax3.text(0.05, 0.7, txt, size='small')
        txt = r"""{{\bf Ingredients}}\\
        IMF: {imf:s}\\
        Isochrone: {iso:s}\\
        Nebular: {neb:s}\\
        Stellib: {stellib:s}"""
        txt = txt.format(imf=oIMF.name, iso=oISO.name, neb=oNEB.name,
                         stellib=oSL.name )
        ax3.text(0.05, 0.35, txt, size='small')
    except Exception as e:
        print(e)

    return ax1, ax2, ax3


def continuous(age=1e8, Z=0.02, fracNeb=1, with_units=True):
    """ Example that generates an ssp model assuming continuous Mass function
    and generates a figure.

    Parameters
    ----------
    age: float, optional units
        age of the population to generate
        default units: 'yr'

    Z: float
        metallicity

    fracNeb: float
        nebular ionizing photon fraction

    with_units: bool
        call get_ssp and request outputs with units
    """

    # Choose ingredients
    # ------------------
    # imf, isochrones, nebular model, spectral library
    #
    oIMF     = imf.Kroupa2001()
    oISO     = isochrones.Pegase()
    # oISO     = isochrones.Padova2010()
    oNEB     = nebular.pegase()
    # oSL      = stellibs.BaSeL()
    oSL      = stellibs.Rauch()  + stellibs.BaSeL()
    # oSL      = stellib.Elodie()

    # Example showing how to add units to quantities
    if not hasUnit(age):
        age *= unit['yr']

    # Running the simulation
    # ----------------------
    # timeit is only used to report how long this block took to execute.
    with timeit('continuous SSP generation'):
        l, s, p = get_ssp(age, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb,
                          inLsun=False,
                          with_units=with_units, 
                          nthreads=0)

    print(p['age'])
    # Make a figure
    # -------------
    with timeit('continuous SSP figure'):
        ssp_plot(l, s, p, oIMF, oISO, oSL, oNEB)

    figure.show()


if __name__ == '__main__':
    continuous()
