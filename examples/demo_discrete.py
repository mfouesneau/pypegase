"""
DEMO - Example of Fully discretly sampled population spectral synthesis

In this example, we generate one population and build a figure representing
it's HR diagram coded by contribution and its spectral models (with and without
nebular contribution).
"""
from __future__ import print_function
import numpy as np
from pypegase.config import __NTHREADS__

from pypegase import imf
from pypegase import isochrones
from pypegase import nebular
from pypegase import stellibs
from pypegase.helpers import val_in_unit, hasUnit, unit
from pypegase.tools import figure, latexFloat, timeit

from pypegase.ssp import discrete as get_ssp


def ssp_plot(l, s, p, oIMF, oISO, oSL, oNEB):
    """
    Plot the HR diagram and the resulting spectra of the SSP generation

    .. note::
        Handles units

    Parameters
    ----------
    l: ndarray, dtype=float
        wavelength of the final spectrum
        units = 'Angstrom'

    s: ndarray, dtype=float
        final spectrum
        units = 'erg/s/AA/lsun' or 'erg/s/AA'

    p: dict
        properties of the population returned by `get_ssp`

    oIMF: IMF instance
        initial mass function, only used for text description

    oISO: Isochrone instance
        isochrone models, only used for text description

    oSL: Stellib instance
        Stellar library, only used for text description

    oNEB: Nebular instance
        nebular model, only used for text description

    Returns
    -------
    ax1: plt.Axes instance
        HR diagram axes

    ax2: plt.Axes instance
        Spectral panel axes

    ax3: plt.Axes instance
        Description panel
    """

    # Handle default units for the plots
    # ----------------------------------
    age = val_in_unit('age', p['age'], 'yr').magnitude
    l = val_in_unit('wavelength', l, 'AA').magnitude
    l0 = val_in_unit('l0', p['l0'], 'AA').magnitude
    if p['inLsun']:
        print(s.unit)
        s = val_in_unit('spectrum', s, 'erg/s/AA/lsun').magnitude
        s0 = val_in_unit('s0', p['s0'], 'erg/s/AA/lsun').magnitude
    else:
        s = val_in_unit('spectrum', s, 'erg/s/AA').magnitude
        s0 = val_in_unit('s0', p['s0'], 'erg/s/AA').magnitude
    mtot = val_in_unit('Mtot', p['Mtot'], 'msun').magnitude

    stars = p['stars']

    figure.figure(figsize=(8, 8))

    # HRdiagram: plot stellib log(g), log(Teff), selected isochrone, ...etc.
    # ----------------------------------------------------------------------
    ax1 = figure.subplot2grid((2, 2), (0, 0), 1, 1)
    # plot the grid
    ax1.plot(oSL.grid['Teff'], oSL.grid['logg'], ',', color='0.0')
    # plot the isochrone
    _iso   = oISO._get_isochrone(val_in_unit('age', p['age'], 'yr'), p['Z'])
    ax1.plot(_iso['logT'], _iso['logg'])
    # plot population and used stars from the spectral library
    Sidx = p['r'][:, 0].astype(int)
    _w   = p['r'][:, 1]
    idx = (_w > 0)
    _w[idx] = np.log10(_w[idx])
    ax1.plot(stars['logT'], stars['logg'], '.', mec='None')
    ax1.scatter(oSL.grid['Teff'][Sidx], oSL.grid['logg'][Sidx], c=_w, edgecolor='None', s=10)
    # polish axis orientations and labels
    ax1.set_xlim(ax1.get_xlim()[::-1])
    ax1.set_ylim(ax1.get_ylim()[::-1])
    ax1.set_ylabel('log(g)')
    ax1.set_xlabel('log(Teff)')

    # Plot the final spectrum (w/ and w/o nebular component)
    # -----------------------------------------------------
    ax2 = figure.subplot2grid((2, 2), (1, 0), 1, 2)
    ax2.loglog(l0, s0)
    ax2.loglog(l, s)
    ax2.set_xlabel(r'Wavelength [$\rm{\AA}$]')
    if p['inLsun']:
        ax2.set_ylabel(r'Flux [erg/s/$\rm{\AA}$/L$_\odot$]')
    else:
        ax2.set_ylabel(r'Flux [erg/s/$\rm{\AA}$]')

    # Add text
    # --------
    try:
        ax3 = figure.subplot2grid((2, 2), (0, 1), 1, 1)
        ax3.axes.set_axis_off()
        txt  = r'{\bf Population characteristics} \\ Age: %s yr' % latexFloat(age, '%0.2g')
        if hasattr(mtot, '__iter__'):
            txt += r'\\ Z: %0.4f \\ Nstars: %s \\ Mtot: %s $\pm$ %s M$_\odot$' % (p['Z'], latexFloat(p['Nstars'], '%0.3g'), latexFloat(np.mean(mtot), '%0.3g'), latexFloat(np.std(mtot), '%0.3g') )
        else:
            txt += r'\\ Z: %0.4f \\ Nstars: %s \\ Mtot: %s M$_\odot$' % (p['Z'], latexFloat(p['Nstars'], '%0.3g'), latexFloat(float(mtot), '%0.3g') )
        ax3.text(0.05, 0.7, txt, size='small')
        ax3.text(0.05, 0.35, r'{\bf Synthesis ingredients}\\IMF: %s \\ Isochrone: %s \\ Nebular: %s \\ Stellib: %s ' % ( oIMF.name, oISO.name, oNEB.name, oSL.name ), size='small')
    except Exception as e:
        print(e)

    return ax1, ax2, ax3


def discrete_plot(ax1, ax2, ax3, l, s, p, oIMF, oISO, oSL, oNEB):
    """ Update axes to add IMF and spectra from multiple draws"""

    # Include units
    # -------------
    if p['inLsun']:
        ds = val_in_unit('ds', p['ds'], 'erg/s/angstrom/lsun').magnitude
    else:
        ds = val_in_unit('ds', p['ds'], 'erg/s/angstrom').magnitude

    _l = val_in_unit('l', l, 'angstrom').magnitude

    # plot multiple spectra on the spectral panel
    # ------------------------------------------
    ax2.loglog(_l, ds.T, alpha=min(1, 4. / len(ds)), color='b')

    # plot the imf
    # ------------
    masses = p['masses'].magnitude
    dm     = np.linspace(0.8 * masses.min(), 1.2 * masses.max(), 100)
    if len(p['Mtot']) > 1:
        phi = np.array([np.histogram(mk, bins=dm, normed=True)[0] for mk in masses ])
    else:
        phi, _  = np.histogram(masses, bins=dm, normed=True)
    # phi    = dn.astype(float) / dm
    m = 0.5 * (dm[:-1] + dm[1:])

    # Plot individual draws
    ax3 = figure.subplot2grid((2, 2), (0, 1), 1, 1)
    if len(p['Mtot']) > 1:
        ax3.loglog(m,   phi.T, alpha=0.3, color='b')
        # plot median value
        y = np.median(phi, axis=0)
        ax3.loglog(m,   y, color='b')
    else:
        ax3.loglog(m,   phi, alpha=0.8, color='b')

    # plot prediciton
    y0 = oIMF(dm)  # new def in dN/dM instead of dN/dlogM
    ax3.loglog(dm, y0, ls=':', color='r', lw=2)
    ax3.set_xlabel('Mass')
    ax3.set_ylabel('dN/dM')
    ax3.set_xlim(masses.min(), masses.max())
    ax3.set_ylim(phi[phi > 0].min(), phi.max())


def discrete(Nstars=int(1e3), age=5e6, Z=0.02, fracNeb=1, nsamp=5, with_units=True):
    """ Example that generate an ssp model assuming according to a purely
    discrete sampling and generates a figure.

    Parameters
    ----------
    Nstars: int
        number of stars in the population

    age: float, optional units
        age of the population to generate
        default units: 'yr'

    Z: float
        metallicity

    fracNeb: float
        nebular ionizing photon fraction

    nsamp: int
        number of populations to generate

    with_units: bool
        call get_ssp and request outputs with units
    """

    # Choose ingredients
    # ------------------
    # imf, isochrones, nebular model, spectral library
    #
    oIMF     = imf.Kroupa93()
    oISO     = isochrones.Pegase()
    # oISO     = isochrones.Padova2010()
    oNEB     = nebular.pegase()
    oSL      = stellibs.BaSeL()
    # oSL      = stellib.Elodie()

    # Example showing how to add units to quantities
    if not hasUnit(age):
        age *= unit['yr']

    # Running the simulation
    # ----------------------
    # timeit is only used to report how long this block took to execute.
    with timeit('Pure discrete SSP generation'):
        l, s, p = get_ssp(Nstars, age, Z, oIMF, oISO, oSL, oNEB,
                          fracNeb=fracNeb, nsamp=nsamp, with_units=with_units,
                          nthreads=__NTHREADS__)

    # Make a figure
    # -------------
    with timeit('Pure discrete figure'):
        ax1, ax2, ax3 = ssp_plot(l, s, p, oIMF, oISO, oSL, oNEB)
        # update to add IMF panel and stochastic sampling
        discrete_plot(ax1, ax2, ax3, l, s, p, oIMF, oISO, oSL, oNEB)

    figure.show()


if __name__ == '__main__':
    discrete()
