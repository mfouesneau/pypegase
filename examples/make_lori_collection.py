"""
DEMO - How to make a collection of models

For this particular application, it becomes highly relevant to try to optimize
the memory and disk operations.

As such, in this file, we create a function based on :func:`ssp.fast_discrete`,
which will attempt to optimize computations. We do not call directly
:func:`ssp.fast_discrete` because it would make a lot of memory usage and
redundant computations.
"""
import numpy as np
from pypegase.io import Table
from pypegase.io.fits import pyfits
from pypegase.config import __NTHREADS__

from pypegase import imf
from pypegase import isochrones
from pypegase import nebular
from pypegase import stellibs
from pypegase import phot
from pypegase.helpers import val_in_unit, hasUnit
from pypegase.units import unit
from pypegase.tools.pbar import Pbar
from pypegase.ssp import genIndividualStellarSpectrum
from pypegase.tools.common import unsecure_histogram as histogram
from pypegase.vega import Vega


def logmass_selection(logmasses, age, intercept=3.537, slope=-0.35):
    """
    From Lori -- The relation that corresponds to F814W = -3 is approximatively,
    log(mass) =  3.537 * log(age) -0.35
    """
    logmlim = intercept + slope * np.log10(age)
    return logmasses <= logmlim


def fast_discrete(nstars, age, Z, imf, iso, sl, neb, fracNeb=0., dlogm=0.001,
                  inLsun=False, with_units=True, full_outputs=False,
                  nthreads=__NTHREADS__):
    """
    Generate an SSP assuming a continuously sampled mass function and
    boostrap the mass binning according to the number of stars.
    The number of samples is set by nsamp

    The approximation we use in this modeling scheme is that stars are
    binned in mass to their continuous equivalent mass bins and represented
    by their central star spectrum.  This approximation is good enough as
    long as variations from on side of the mass bin to the other are
    negligeable.

    Parameters
    ----------
    nstars: sequence
        total number of stars per population
        e.g., (1e3, 1e4, 5, 10) will run 4 simulations

    age: float
        age of the population (in yr)

    Z: float
        metallicity

    imf: imf.Imf instance
        Initial Mass function

    iso: isochrone.Isochrone instance
        Isochrone library

    sl: stellib.Stellib instance
        stellar library

    neb: nebular.Nebular instance
        Nebular Model

    fracNeb: float
        Photon fraction tranformed into nebular component

    nsamp: int
        number of mass sampling

    inLsun: bool
        return results units normalized to Lsun

    with_units: bool
        if set, return results with units

    full_outputs: bool
        if set, returns also the individual stars and their frequencies

    Returns
    -------
    l: numpy.ndarray[float, ndim=1]
        wavelength in AA

    s: numpy.ndarray[float, ndim=1]
        Spectrum in ergs/s/AA or ergs/s/AA/Lsun

    p: dict
        various population properties

    stars: Table
        table of stars describing the isochrone
        only if full_output is set

    specs: ndarray
        spectra associated to each star in stars
        only if full_output is set

    dN: ndarray
        number of stars represented by each of the ones in stars
        only if full_output is set
    """
    # check units
    _age = val_in_unit('age', age, 'yr').magnitude

    Lsun = float(unit['lsun'].to('erg/s').magnitude)

    # Continuously sample the corresponding isochrone
    # and simulate the continuous population
    # to set the different variables and dimensions, respectively
    # -----------------------------------------------------------
    # get reference stars for a continuously sampled isochrone
    stars, logm_bins = iso._get_continuous_isochrone(_age, Z, ret_bins=True,
                                                     dlogm=dlogm)
    m_bins = 10 ** logm_bins
    # try to add robustness
    if (np.diff(m_bins) < 0).any():
        raise Exception('binning error')

    # get the predicted number of stars per bin of mass on this isochrone
    # dN = imf.nstars(stars['logM'], ret_bins=False, edges=logm_bins)
    maxM = 10 ** stars['logM'].max()

    # compute all the individual spectra incl. neb component.
    # this will allow us to to a simple weighted sum.
    stars_lamb, stars_specs, NHI = genIndividualStellarSpectrum(stars, Z, sl,
                                                                inLsun=inLsun)
    # add the nebular emission
    if (neb is not None) & (fracNeb > 0):
        if inLsun:
            l, s = neb.add_emission_to_many(stars_lamb * unit['angstrom'],
                                            stars_specs * Lsun, NHI, fracNeb)
            s /= Lsun
        else:
            l, s = neb.add_emission_to_many(stars_lamb * unit['angstrom'],
                                            stars_specs, NHI, fracNeb)
    else:
        l = stars_lamb
        s = stars_specs

    # book keeping variables
    nsamp = len(nstars)
    specs = np.zeros((nsamp, l.size), dtype=float)
    Nstars = np.zeros(nsamp, dtype=float)
    Mtot = np.zeros(nsamp, dtype=float)
    Minit = np.zeros(nsamp, dtype=float)
    Mdead = np.zeros(nsamp, dtype=float)
    Mmax = np.zeros(nsamp, dtype=float)
    Lbol = np.zeros(nsamp, dtype=float)
    age = np.ones(nsamp, dtype=float) * _age
    Z = np.ones(nsamp, dtype=float) * Z

    # special book keeping
    specs_lori = np.zeros((nsamp, l.size), dtype=float)
    Nstars_lori = np.zeros(nsamp, dtype=float)
    Mtot_lori = np.zeros(nsamp, dtype=float)
    Mmax_lori = np.zeros(nsamp, dtype=float)
    Lbol_lori = np.zeros(nsamp, dtype=float)

    if full_outputs:
        dN = np.zeros((nsamp, len(stars)), dtype=float)

    for k, Nk in Pbar(nsamp, desc='Mixing').iterover(enumerate(nstars)):
        # Mass properties of the populations
        # ----------------------------------
        _masses = imf.random(int(Nk))

        # Approximation scheme
        # --------------------
        _dN = histogram( _masses, m_bins)[0].astype(float)

        Nstars[k] = Nk
        specs[k, :] = (_dN[:, None] * s).sum(axis=0)
        Lbol[k] = (10 ** stars['logL'] * _dN).sum()
        ind = (_masses > maxM)
        # total mass above the isochrone limit
        Mdead[k] = np.sum(_masses[ind])
        Minit[k] = np.sum(_masses)
        Mmax[k] = np.max(_masses)
        Mtot[k] = np.sum(_masses[~ind])

        keep = logmass_selection(np.log10(_masses), _age)
        _dN_lori = histogram(_masses[keep], m_bins)[0].astype(float)
        Nstars_lori[k] = sum(keep)
        specs_lori[k, :] = (_dN_lori[:, None] * s).sum(axis=0)
        Mtot_lori[k] = np.sum(_masses[keep])
        Mmax_lori[k] = np.max(_masses[keep])
        Lbol_lori[k] = (10 ** stars['logL'] * _dN_lori).sum()

        if full_outputs:
            dN[k, :] = _dN

    if with_units:
        l = l * unit['angstrom']
        s = s * unit['erg/s/angstrom']
        specs = specs * unit['erg/s/angstrom']
        specs_lori = specs_lori * unit['erg/s/angstrom']

        if inLsun:
            specs = specs / unit['lsun']
            specs_lori = specs_lori / unit['lsun']
            s = s / unit['lsun']

        properties_lori = dict(Mtot=Mtot_lori * unit['msun'],
                               Minit=Minit * unit['msun'],
                               Mdead=Mdead * unit['msun'],
                               Nstars=Nstars_lori,
                               Mmax=Mmax_lori * unit['msun'],
                               Lbol=Lbol_lori * unit['lsun'],
                               age=age * unit['yr'],
                               Z=Z)
        properties = dict(Mtot=Mtot * unit['msun'],
                          Minit=Minit * unit['msun'],
                          Mdead=Mdead * unit['msun'],
                          Nstars=Nstars,
                          Mmax=Mmax * unit['msun'],
                          Lbol=Lbol * unit['lsun'],
                          age=age * unit['yr'],
                          Z=Z)
    else:
        properties = dict(Mtot=Mtot, Minit=Minit, Mdead=Mdead, Mmax=Mmax,
                          Lbol=Lbol, age=age, Z=Z, Nstars=Nstars)
        properties_lori = dict(Mtot=Mtot_lori, Minit=Minit, Mdead=Mdead,
                               Mmax=Mmax_lori, Lbol=Lbol_lori, age=age, Z=Z,
                               Nstars=Nstars_lori)

    if full_outputs:
        return l, specs, properties, specs_lori, properties_lori, stars, s, dN
    else:
        return l, specs, properties, specs_lori, properties_lori


def save_samples(fname, p, l, s):

    def unit_drop(q, u=None):
        """ Drop units or assume its ok """
        if hasUnit(q):
            if u is not None:
                return q.to(u).magnitude
            else:
                return q.magnitude
        else:
            return q

    data = np.vstack([unit_drop(s), unit_drop(l, 'AA')])

    header = pyfits.Header()
    header['SPEC_UNIT'] = '{0:s}'.format(str(s.units))
    header['LAMB_UNIT'] = '{0:s}'.format(str(l.units))
    pyfits.writeto(fname, data, header=header)

    data = {}
    header = {}
    for k, v in p.items():
        data[k] = unit_drop(v)
        if hasUnit(v):
            header['{0:s}_UNIT'] = str(v.units)

    Table(data, header=header).write(fname, append=True)


def save_pops(fname, stars, l, s, dN):

    def unit_drop(q, u=None):
        """ Drop units or assume its ok """
        if hasUnit(q):
            if u is not None:
                return q.to(u).magnitude
            else:
                return q.magnitude
        else:
            return q

    data = np.vstack([unit_drop(s), unit_drop(l, 'AA')])

    header = pyfits.Header()
    header['SPEC_UNIT'] = '{0:s}'.format(str(s.units))
    header['LAMB_UNIT'] = '{0:s}'.format(str(l.units))
    pyfits.writeto(fname, data, header=header)
    pyfits.append(fname, dN)

    stars.write(fname, append=True)


def extract_photometry(fname, filters=None, mag=True, vega=True):
    """ Extract photometry from a collection """
    f = pyfits.open(fname)
    specs = f[0].data[:-1]
    lambs = f[0].data[-1]
    f.close()
    data = Table(fname)

    if filters is None:
        filters = ['HST_WFC3_F275W', 'HST_WFC3_F336W', 'HST_ACS_WFC_F475W',
                   'HST_ACS_WFC_F814W', 'HST_WFC3_F110W', 'HST_WFC3_F160W']

    flist = phot.load_filters(filters, lamb=lambs)
    cl, flux = phot.extractPhotometry(lambs, specs, flist)
    unit = 'erg/s/cm2'
    desc = None

    if vega is True:
        vf = Vega().getFlux(filters)[1]
        flux = flux / vf[None, :]
        desc = 'normalized to vega'

    if mag is True:
        flux = -2.5 * np.log10(flux)
        unit = 'mag'

    for ek, fk in enumerate(flist):
        data.add_column(fk.name.split('_')[-1], flux[:, ek], unit=unit,
                        description=desc)

    return data


def merge_collection_ouputs(fname, pattern, chunksize=10):
    """ Merge spectral outputs into one giant file

    Parameters
    ----------

    fname: str
        output name of the final collection

    chunksize: int
        proceed to merging arrays every chunksize number of files
        if <= 0, proceed all at the end
    """
    from glob import glob

    lst = list(glob(pattern))

    if chunksize <= 0:
        chunksize = len(lst) + 1

    # loading first file
    f = pyfits.open(lst[0])
    lambs = f[0].data[-1]
    specs = [f[0].data[:-1]]
    props = Table(lst[0])
    header = f[0].header.copy()
    f.close()

    for ek, fk in Pbar(len(lst) - 1, desc='merging').iterover(enumerate(lst[1:])):
        f = pyfits.open(lst[0])
        specs.append(f[0].data[:-1])
        f.close()
        del f
        props.stack(Table(fk))
        if (ek % chunksize) == 0:
            specs = [np.vstack(specs)]

    # finish format
    print('writing to disk')
    specs.append(lambs)
    specs = np.vstack(specs)

    pyfits.writeto(fname, specs, header=header)
    props.write(fname, append=True)
    print('Merging done into {0}'.format(fname))


def main(age, Nstars=int(1e6), Z=0.02, fracNeb=1):

    oIMF     = imf.Kroupa93()
    oISO     = isochrones.Pegase()
    oNEB     = nebular.pegase()
    oSL      = stellibs.BaSeL()

    # for age in Pbar('Ages').iterover(oISO.ages):
    l, s, p, s1, p1, stars, stars_spec, dN = fast_discrete(Nstars, age, Z, oIMF,
                                                           oISO, oSL, oNEB,
                                                           fracNeb=fracNeb,
                                                           dlogm=0.01,
                                                           full_outputs=True,
                                                           nthreads=__NTHREADS__)

    fname = 'collection_age_{0:d}.fits'.format(int(age * 1e-6))
    save_samples(fname, p, l, s)
    fname = 'collection_lori_age_{0:d}.fits'.format(int(age * 1e-6))
    save_samples(fname, p1, l, s1)
    fname = 'collection_pops_age_{0:d}.fits'.format(int(age * 1e-6))
    save_pops(fname, stars, l, stars_spec, dN)

if __name__ == '__main__':
    Nstars = 10 ** np.random.uniform(2, 4, 100)
    main(1e7, Nstars=Nstars, Z=0.02, fracNeb=1)
