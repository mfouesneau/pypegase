"""
DEMO - Do a few pseudo-discrete spectra
=======================================

This example goes step by step through the process of generating
pseudo-discretly sampled populations with various ages and plot them
on a single figure.
pseudo-discrete means approximate each mass by the closest mass from
a continuously sampled isochrone.
"""
import pylab as plt
import numpy as np
from pypegase.tools import latexFloat
from pypegase.tools import figrc
#from pypegase.tools import timeit

# pypegase imports
from pypegase import imf
from pypegase import isochrone
from pypegase import nebular
from pypegase import stellib
from pypegase.ssp import fast_discrete
from pypegase.ssp import continuous
from pypegase.units import unit

from itertools import cycle

marble = False
marble = True

Lsun = float(unit['lsun'].to('erg/s').magnitude)

import gc

gc.enable()


#ages = [1e6, 5e6, 1e7, 1e8, 1e9]
ages = [3e6, 1e7, 1e8]
Z = 0.02
fracNeb = .0
Nstars = int(1e7)


if marble:
    import pyfits

    marble_f = pyfits.open('tests/spectra_krou01_NGVS_Z0.02_noneb.fits')
    marble_ages = marble_f[2].data['AGE']
    marble_masses = marble_f[2].data['MGAL']

    marble_lamb = []
    marble_spec = []
    for ak in ages:
        ind = np.argmin((marble_ages * 1e6 - ak) ** 2)
        print ind
        print 'ages: ', ak, marble_ages[ind]
        h = marble_f[0].data[ind] * Lsun

        marble_lamb.append(marble_f[3].data[:])
        marble_spec.append(h / marble_masses[ind])


""" Generate an ssp model assuming continuous Mass function """
oIMF     = imf.Kroupa2001()
#oIMF     = imf.Kroupa93()
oISO     = isochrone.pegase()
#oISO     = isochrone.padova2010()
oNEB     = None   # nebular.pegase()
oSL      = stellib.BaSeL()
#oSL      = stellib.Elodie()


# Compute continuous spectra by looping through values in ages
# ------------------------------------------------------------
ln = []
sn = []
for age in ages:
    lp, sp, pp = fast_discrete(Nstars, age * unit['yr'], Z, oIMF, oISO, oSL,
                               oNEB, fracNeb=fracNeb, nsamp=1, nthreads=2,
                               with_units=False)
    ln.append(lp)
    sn.append(sp / float(pp['Minit']))
    #print(pp['Mtot'], pp['Mdead'], pp['Mdead'] / pp['Mtot'])
    gc.collect()

# Compute continuous spectra by looping through values in ages
# ------------------------------------------------------------
lc = []
sc = []
for age in ages:
    l0, s0, p0 = continuous(age * unit['yr'], Z, oIMF, oISO, oSL, oNEB,
                            fracNeb=fracNeb, nthreads=2, inLsun=False,
                            with_units=False)
    lc.append(l0)
    sc.append(s0)
    #print(p0['Mtot'])
    gc.collect()


# Make the figure
# ----------------
#Plot the final spectrum (w/ and w/o nebular component)
figrc.ezrc(20, 1, 20)
plt.figure(figsize=(10, 10))

ax = plt.subplot(211)

# plot pseudo-discrete
colors = cycle('bgrcmyk')
for ak, lk, sk, color in zip(ages, ln, sn, colors):
    ax.loglog(lk, sk, label='{0} yr'.format(latexFloat(ak)), lw=2, color=color)

# plot continuous
colors = cycle('bgrcmyk')
for ak, lk, sk, color in zip(ages, lc, sc, colors):
    ax.loglog(lk, sk, label='{0} yr'.format(latexFloat(ak)), lw=2, ls=':', color=color)

#plot marble
if marble:
    for mlk, msk in zip(marble_lamb, marble_spec):
        ax.loglog(mlk, msk, color='k')

if p0['inLsun']:
    ax.set_ylabel(r'Flux [erg/s/$\rm{\AA}$/L$_\odot$/M$_\odot$]')
    ax.set_ylim(1e-7, 1.)
else:
    ax.set_ylabel(r'Flux [erg/s/$\rm{\AA}$/M$_\odot$]')
    ax.set_ylim(1e26, 1e36)

#polish the figure
plt.legend(loc='upper right', title='Ages', fontsize=10).draw_frame(False)
figrc.hide_axis(['top', 'right'], ax=ax)

ax = plt.subplot(212, sharex=ax)

plt.hlines([0], min(lk), max(lk), colors='k', linestyles=':')

# plot pseudo-discrete
colors = cycle('bgrcmyk')
for ak, lk, sk, sck, color in zip(ages, ln, sn, sc, colors):
    ax.semilogx(lk, 100. * (sk - sck) / sck, label='{0} yr'.format(latexFloat(ak)), lw=2, color=color)

#plot marble
if marble:
    for mlk, msk, lk, sk in zip(marble_lamb, marble_spec, ln, sn):
        ax.semilogx(lk, 100. * (msk - sk) / sk, color='k')

    for mlk, msk, lk, sck in zip(marble_lamb, marble_spec, lc, sc):
        ax.semilogx(lk, 100. * (msk - sck) / sck, color='k', ls=':')


ax.set_xlabel(r'Wavelength [$\rm{\AA}$]')
ax.set_ylabel(r'Relative error to Conti. [\%]')
figrc.hide_axis(['top', 'right'], ax=ax)
ax.set_xlim(800, 1e5)

if marble:
    marble_f.close()

plt.subplots_adjust(hspace=0.12)
plt.show()
