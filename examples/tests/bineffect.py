""" This script tests the binning effect on the fast discrete generation of models """
import numpy
from tools import timeit
import imf
import isochrone
import stellib


def main():
    iso     = isochrone.pegase()
    osl     = stellib.BaSeL()
    oIMF    = imf.Kroupa2001()

    age      = 1e7
    Z        = 0.02

    #get binning of the isochrone
    with timeit('resample the isochrone'):
        stars0      = iso._get_continuous_isochrone(age, Z)
        masses     = numpy.power(10., stars0['logM'])
        dN, bins   = oIMF.nstars1(stars0['logM'], dlogm=stars0['dlogm'], ret_bins=True)
        minM, maxM = 10 ** stars0['logM'].min(), 10 ** stars0['logM'].max()
        bins       = 10 ** bins
        m          = numpy.hstack([masses, bins])

    #use bin edges as new stars
    with timeit('Compute spectra of mass bin edges'):
        stars            = iso._get_isochrone(age, Z, masses=m)

        l0  = osl.wavelength
        s = numpy.empty( (stars.nrows, len(l0) ), dtype=float )

        for k in range(stars.nrows):
            sk       = stars[k]
            r        = osl.interp(sk['logT'], sk['logg'], Z, sk['logL'], dT_max=0.1, eps=1e-6)
            s[k, :]   = osl.genSpectrum(numpy.asarray(r).T)
            del r, sk

    return l0, s, stars
