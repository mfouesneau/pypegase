import tables
import numpy as np
from imf import Kroupa2001

f = tables.openFile('libs/pegase.iso.hd5')
root = f.root.Z02
isos = f.root.Z02._v_children
r = np.empty((len(isos), 2), dtype=float)

for e, k in enumerate(isos.values()):
    r[e] = k.attrs['time'], k.cols.logM[:].max()

ind = np.argsort(r[:, 0])
t = r[:, 0][ind]
logMmax = r[:, 1][ind]

logMmax = logMmax[t > 0]
logt = np.log10(t[t > 0])

masses = np.empty(len(logt), dtype=float)
nstars = np.empty(len(logt), dtype=float)

oimf = Kroupa2001()
for e, k in enumerate(logMmax):
    masses[e] = oimf.get_enclosed_mass(10 ** k, oimf.massMax)
    nstars[e] = oimf.get_enclosed_Nstar(10 ** k, oimf.massMax)

masses /= oimf.get_enclosed_mass(oimf.massMin, oimf.massMax)
nstars /= oimf.get_enclosed_Nstar(oimf.massMin, oimf.massMax)
