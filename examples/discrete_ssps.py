""" This will be a single stellar population synthetizer soon """


""" tasks:

	* define an IMF
	* define isochrone set
	* define stellib set

	* get masses from IMF
	* get associated log(Teff), log(g), log(L) from isochrones
	* get template stars by interpolating stellar library
	* get emission fluxes
"""


import numpy
import inspect, os

import extinction, imf, isochrone, nebular, stellib
from tools import *
from config import __NTHREADS__, printConfig

localpath = '/'.join(os.path.abspath(inspect.getfile(inspect.currentframe())).split('/')[:-1])

printConfig()

def plot(oIMF, oISO, oSL, oNEB, Nstars, age, Z, fracNeb, masses, pop, r, l0, s0, l, s):
	#Plot a log(T), log(g) diagram and associated spectrum
	import pylab as figure

	figure.figure(1, figsize = (8,8))
	ax1 = figure.subplot2grid((2,2), (0,0), 1, 1)

	##plot Stellib
	ax1.plot(oSL.grid['Teff'], oSL.grid['logG'], ',', color='0.0')

	##plot Isochrone
	_iso   = oISO._get_isochrone(age, Z)
	ax1.plot(_iso['logT'], _iso['logg'])
	ax1.set_xlim(ax1.get_xlim()[::-1])
	ax1.set_ylim(ax1.get_ylim()[::-1])
	ax1.set_ylabel('log(g)')
	ax1.set_xlabel('log(Teff)')

	##plot population and selected stars from SL
	Sidx = r[:,0].astype(int)
	ax1.plot(pop['logT'], pop['logg'], 'o')
	ax1.scatter(oSL.grid['Teff'][Sidx], oSL.grid['logG'][Sidx], c=r[:,1], edgecolor='None')

	ax2 = figure.subplot2grid((2,2), (1,0),1,2)
	ax2.loglog(l0, s0)
	ax2.loglog(l,   s)
	ax2.set_xlabel(r'Wavelength [$\rm{\AA}$]')
	ax2.set_ylabel(r'Flux [F/F$_\odot$]')

	ax3 = figure.subplot2grid((2,2), (0,1),1,1)
	ax3.axes.set_axis_off()
	ax3.text(0.05, 0.7, r'{\bf Population characteristics} \\ Age: %0.2f Myr \\ Z: %0.5f \\ Nstars: %0.3g \\ Mtot: %0.3g M$_\odot$' % (age*1e-6, Z, Nstars, masses.sum()) , size='small')

	ax3.text(0.05, 0.35, r'{\bf Synthesis ingredients}\\IMF: %s \\ Isochrone: %s \\ Nebular: %s \\ Stellib: %s ' % ( oIMF.name, oISO.name, oNEB.name, oSL.name ), size='small')
	figure.show()


def ssps(oIMF, oISO, oSL, oNEB, Nstars, age, Z, fracNeb, nthreads=__NTHREADS__):
	""" Generate an SSP """
	masses = oIMF.random(Nstars)
	pop    = oISO._get_isochrone(age, Z, masses = masses)
	r      = oSL.interpMany(pop['logT'], pop['logg'], Z, nthreads=nthreads)
	l0     = oSL.wavelength
	s0     = oSL.genSpectrum(r)
	NHI    = oSL.genQ('NHI', r)
	l, s   = oNEB.add_emission(l0, s0, NHI, fracNeb, mass=masses.sum())
	return masses, pop, r, l0, s0, l, s


oIMF     = imf.Kroupa2001()
oISO     = isochrone.pegase()
oNEB     = nebular.pegase()
#oSL      = stellib.BaSeL()
oSL      = stellib.Elodie()

Nstars   = int(5e4)
age      = 5e7
Z        = 0.02
fracNeb  = 1.

nthreads = __NTHREADS__

with timeit('SSP generation'):
	masses, pop, r, l0, s0, l, s = ssps(oIMF, oISO, oSL, oNEB, Nstars, age, Z, fracNeb = fracNeb, nthreads=nthreads)
plot(oIMF, oISO, oSL, oNEB, Nstars, age, Z, fracNeb, masses, pop, r, l0, s0, l, s)

