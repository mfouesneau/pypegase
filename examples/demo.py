"""
DEMO - EXAMPLES
"""
import numpy
from tools import figure, latexFloat, timeit


def ssp_plot(l, s, p, oIMF, oISO, oSL, oNEB):
    """
    Plot the HR diagram and the resulting spectra of the SSP generation
    """
    #import pylab as figure
    stars = p['stars']

    figure.figure(figsize=(8, 8))
    #plot stellib log(g), log(Teff), the selected isochrone, and more
    ax1 = figure.subplot2grid((2, 2), (0, 0), 1, 1)
    ## plot the grid
    ax1.plot(oSL.grid['Teff'], oSL.grid['logG'], ',', color='0.0')
    ## plot the isochrone
    _iso   = oISO._get_isochrone(p['age'], p['Z'])
    ax1.plot(_iso['logT'], _iso['logg'])
    ##plot population and used stars from the spectral library
    Sidx = p['r'][:, 0].astype(int)
    _w   = p['r'][:, 1]
    idx = (_w > 0)
    _w[idx] = numpy.log10(_w[idx])
    ax1.plot(stars['logT'], stars['logg'], '.', mec='None')
    ax1.scatter(oSL.grid['Teff'][Sidx], oSL.grid['logG'][Sidx], c=_w, edgecolor='None', s=10)
    ##polish axis orientations and labels
    ax1.set_xlim(ax1.get_xlim()[::-1])
    ax1.set_ylim(ax1.get_ylim()[::-1])
    ax1.set_ylabel('log(g)')
    ax1.set_xlabel('log(Teff)')

    #Plot the final spectrum (w/ and w/o nebular component)
    ax2 = figure.subplot2grid((2, 2), (1, 0), 1, 2)
    ax2.loglog(p['l0'], p['s0'])
    ax2.loglog(l, s)
    ax2.set_xlabel(r'Wavelength [$\rm{\AA}$]')
    if p['inLsun']:
        ax2.set_ylabel(r'Flux [erg/s/$\rm{\AA}$/L$_\odot$]')
    else:
        ax2.set_ylabel(r'Flux [erg/s/$\rm{\AA}$]')

    #Add text (better in Latex)
    try:
        ax3 = figure.subplot2grid((2, 2), (0, 1), 1, 1)
        ax3.axes.set_axis_off()
        txt  = r'{\bf Population characteristics} \\ Age: %s yr' % latexFloat(p['age'], '%0.2g')
        txt += r'\\ Z: %0.4f \\ Nstars: %s \\ Mtot: %s M$_\odot$' % (p['Z'], latexFloat(p['Nstars'], '%0.3g'), latexFloat(float(p['Mtot']), '%0.3g') )
        ax3.text(0.05, 0.7, txt, size='small')
        ax3.text(0.05, 0.35, r'{\bf Synthesis ingredients}\\IMF: %s \\ Isochrone: %s \\ Nebular: %s \\ Stellib: %s ' % ( oIMF.name, oISO.name, oNEB.name, oSL.name ), size='small')
    except:
        pass
    return ax1, ax2, ax3


@timeit
def continuous():
    """ Generate an ssp model assuming continuous Mass function """

    from config import __NTHREADS__
    from tools import timeit

    import imf
    import isochrone
    import nebular
    import stellib
    from ssp import continuous as get_ssp

    oIMF     = imf.Kroupa2001()
    oISO     = isochrone.pegase()
    #oISO     = isochrone.padova2010()
    oNEB     = nebular.pegase()
    oSL      = stellib.BaSeL()
    #oSL      = stellib.Elodie()

    #age      = 10**6.9901
    #age      = 280.*1e6
    #age      = 5e9
    age      = 1e6
    Z        = 0.02
    fracNeb  = 1.

    with timeit('continuous SSP generation'):
        l, s, p = get_ssp(age, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb, nthreads=__NTHREADS__)

    ssp_plot(l, s, p, oIMF, oISO, oSL, oNEB)
    figure.show()


def fast_discrete_plot(ax1, ax2, ax3, l, s, p, oIMF, oISO, oSL, oNEB):
    #add variations
    ds = p['ds']
    for k in range(len(ds)):
        ax2.loglog(l, ds[k], alpha=4. / len(ds), color='b')

    #plot the imf
    masses = numpy.power(10., p['stars']['logM'])
    dm     = p['dm']
    idx    = numpy.where(dm > 0)[0]
    dn     = p['dn'][:, idx].astype(float)  # dn/dbin = dn
    masses = masses[idx]
    dm     = dm[idx]
    phi    = dn / dm
    ax3 = figure.subplot2grid((2, 2), (0, 1), 1, 1)
    ax3.loglog(masses,   phi.T, alpha=1. / len(dn), color='b')
    y = numpy.median(phi, axis=0)
    ax3.loglog(masses,   y, color='b')
    y0 = oIMF(masses) / masses
    ax3.loglog(masses, y0 / y0[10] * y[10], ls=':', color='r', lw=2)
    ax3.set_xlabel('Mass')
    ax3.set_ylabel('dN/dM')
    ax3.set_xlim(masses.min(), ((dn.max(axis=0) > 0 ).astype(int) * masses).max())
    ax3.set_ylim(phi[phi > 0].min(), phi.max())


def fast_discrete():
    from config import __NTHREADS__
    from tools import timeit

    import imf
    import isochrone
    import nebular
    import stellib
    from ssp import fast_discrete as get_ssp

    oIMF     = imf.Kroupa2001()
    oISO     = isochrone.pegase()
    oNEB     = nebular.pegase()
    oSL      = stellib.BaSeL()
    #oSL      = stellib.Elodie()

    Nstars   = int(1e6)
    age      = 1e6
    #age      = 280.*1e6
    Z        = 0.02
    fracNeb  = 1.
    nsamp    = 4

    with timeit('Pseudo-discrete SSP generation'):
        l, s, p = get_ssp(Nstars, age, Z, oIMF, oISO, oSL, oNEB,
                          fracNeb=fracNeb, dlogm=0.01, nsamp=nsamp, nthreads=__NTHREADS__)

    ax1, ax2, ax3 = ssp_plot(l, s, p, oIMF, oISO, oSL, oNEB)
    fast_discrete_plot(ax1, ax2, ax3, l, s, p, oIMF, oISO, oSL, oNEB)
    figure.show()
    return p


def discrete_plot(oIMF, oISO, oSL, oNEB, Nstars, age, Z, fracNeb, masses, pop, r, l0, s0, l, s):
    #Plot a log(T), log(g) diagram and associated spectrum
    figure.figure(1, figsize=(8, 8))
    ax1 = figure.subplot2grid((2, 2), (0, 0), 1, 1)

    ##plot Stellib
    ax1.plot(oSL.grid['Teff'], oSL.grid['logG'], ',', color='0.0')

    ##plot Isochrone
    _iso   = oISO._get_isochrone(age, Z)
    ax1.plot(_iso['logT'], _iso['logg'])
    ax1.set_xlim(ax1.get_xlim()[::-1])
    ax1.set_ylim(ax1.get_ylim()[::-1])
    ax1.set_ylabel('log(g)')
    ax1.set_xlabel('log(Teff)')

    ##plot population and selected stars from SL
    Sidx = r[:, 0].astype(int)
    ax1.plot(pop['logT'], pop['logg'], 'o')
    ax1.scatter(oSL.grid['Teff'][Sidx], oSL.grid['logG'][Sidx], c=r[:, 1], edgecolor='None')

    ax2 = figure.subplot2grid((2, 2), (1, 0), 1, 2)
    ax2.loglog(l0, s0)
    ax2.loglog(l,   s)
    ax2.set_xlabel(r'Wavelength [$\rm{\AA}$]')
    ax2.set_ylabel(r'Flux [F/F$_\odot$]')

    ax3 = figure.subplot2grid((2, 2), (0, 1), 1, 1)
    ax3.axes.set_axis_off()
    ax3.text(0.05, 0.7, r'{\bf Population characteristics} \\ Age: %0.2f Myr \\ Z: %0.5f \\ Nstars: %0.3g \\ Mtot: %0.3g M$_\odot$' % (age * 1e-6, Z, Nstars, masses.sum()), size='small')

    ax3.text(0.05, 0.35, r'{\bf Synthesis ingredients}\\IMF: %s \\ Isochrone: %s \\ Nebular: %s \\ Stellib: %s ' % ( oIMF.name, oISO.name, oNEB.name, oSL.name ), size='small')
    figure.show()



#continuous()
#fast_discrete()
