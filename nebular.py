""" Everything nebular continuum + emission related

nebular emission lines relative to the underlying stellar continuum indicating
that the stars and gas that give rise to the lines and to the continuum see
different amounts of dust. This interaction between stars and gas led to
two-step model in which young stars which emit ionizing photons are likely to
be still surrounded by the clouds of gas and dust from which they formed. In
this model all stars are attenuated by "diffuse" dust in the same manner as
equation 2. However young ( < 10 Myr) stars undergo an additional "birth cloud"
attenuation.

In practice this means that the UV light and nebular emission lines associated
with the short-lived massive stars are more obscured than the optical light
dominated by the longer-lived stars, as observed in real galaxies

Finally the integrated spectrum of a given SSP is the mixture of a stellar
content interacting with dust and gas.
"""
import numpy as np
from numpy import interp
from .io import Table
from .units import unit, hasUnit
from .helpers import val_in_unit
from .tools.compatibility import range
from .tools.pbar import Pbar

import inspect
import os

localpath = '/'.join(os.path.abspath(inspect.getfile(inspect.currentframe())).split('/')[:-1])


class NebularModel(object):
    """ template Class """
    def __init__(self, name='', *args, **kwargs):
        self.name = name

    def gen_spectra(self, lamb, *args, **kwargs):
        """ template """
        raise NotImplemented

    def add_emission(self, lamb, flux, NHI, frac, with_units=False, **kwargs):
        """
        Compute the nebular component and add its contribution

        Parameters
        ----------
        lamb: ndarray, dtype=float
            initial wavelength of the spectrum
            in angstroms

        flux: ndarray, dtype=float
            initial flux of the spectrum

        NHI: int
            number of photons contributing to HI Lym_alpha

        frac: float
            fraction of ionizing photons

        with_units: bool
            if set, return results with units

        Returns
        -------
        l: ndarray, dtype=float
            wavelength values revised from the input array to include emission lines
            in angstroms

        f: ndarray, dtype=float
            final associated fluxes
            in units of input fluxes

        .. note::

            kwargs forwarded to self.gen_spectra
        """
        u_lamb = val_in_unit('lamb', lamb, 'angstrom')
        _lamb = u_lamb.magnitude

        if not hasUnit(flux):
            f_unit = unit['erg/s/AA']
        else:
            f_unit = unit[flux.units]

        nhi = val_in_unit('N(HI)', NHI, '1/s').magnitude

        lyman_alpha_sup = 912.  # AA
        # get the nebular component
        le, fe = self.gen_spectra(u_lamb, with_units=False, **kwargs)
        # make sure the spectra are def on the same wavelengths
        f      = interp(le, _lamb, flux)
        # Do the actual sum
        f += nhi * frac * fe
        # remove photons absorbed if spectrum contain it
        ind = np.where(le < lyman_alpha_sup)[0]
        if ( len(ind) > 0 ):
            f[ind] *= 1. - frac

        if with_units:
            return le * unit['AA'], f * f_unit
        else:
            return le, f

    def add_emission_to_many(self, lamb, flux, NHI, frac, with_units=False, **kwargs):
        """ Compute the nebular component and add its contribution

        Parameters
        ----------
        lamb: ndarray, dtype=float
            initial wavelength of the spectrum
            in angstroms

        flux: ndarray, dtype=float
            initial flux of the spectrum

        NHI: int
            number of photons contributing to HI Lym_alpha

        frac: float
            fraction of ionizing photons

        with_units: bool
            if set, return results with units

        .. note::

            kwargs forwarded to self.gen_spectra

        Returns
        -------
        l: ndarray, dtype=float
            wavelength values revised from the input array to include emission lines
            in angstroms

        f: ndarray, dtype=float
            final associated fluxes
            in units of input fluxes
        """
        u_lamb = val_in_unit('lamb', lamb, 'angstrom')
        _lamb = u_lamb.magnitude

        if not hasUnit(flux):
            f_unit = unit['erg/s/AA']
        else:
            f_unit = unit[flux.units]

        nhi = val_in_unit('N(HI)', NHI, '1/s/cm**2').magnitude

        silent = kwargs.pop('silent', False)

        lyman_alpha_sup = 912.  # AA
        # get the nebular component
        le, fe = self.gen_spectra(u_lamb, with_units=False, **kwargs)
        # remove photons absorbed if spectrum contain it
        ind = np.where(le < lyman_alpha_sup)[0]

        r = []
        if silent:
            for fk, nk in zip(flux, nhi):
                # make sure the spectra are def on the same wavelengths
                f = interp(le, _lamb, fk)
                # Do the actual sum
                f += nk * frac * fe
                if ( len(ind) > 0 ):
                    f[ind] *= 1. - frac
                r.append(f)
        else:
            for fk, nk in Pbar(len(nhi), desc='Nebulosity').iterover(zip(flux, nhi)):
                # make sure the spectra are def on the same wavelengths
                f = interp(le, _lamb, fk)
                # Do the actual sum
                f += nk * frac * fe
                if ( len(ind) > 0 ):
                    f[ind] *= 1. - frac
                r.append(f)

        f = np.array(r)

        if with_units:
            return le * unit['AA'], f * f_unit
        else:
            return le, f


class Pegase(NebularModel):
    """
    Nebular model implemented in Pegase.2

    A simplified nebular emission model computed as in Guiderdoni et al. (1987)
    is linked to the number of ionizing photons in the specific galaxy model,
    furnishing line intensities and the nebular continuum in the same
    wavelength range as the stellar spectra. The models also include the
    effects of dust extinction using the average properties for the galaxy type
    and the chemical enrichment of the interstellar medium.

    .. note::

        requires local resources

            * libs/pegase.HII.cont.fits
            * libs/pegase.HII.lines.fits
    """
    def __init__(self, name=None):
        if name is None:
            name = 'Pegase model (Fioc+1997)'

        NebularModel.__init__(self, name)

        # libraries
        self.continuumFile = localpath + '/libs/pegase.HII.cont.fits'
        self.linesFile     = localpath + '/libs/pegase.HII.lines.fits'

    def _get_continuum_(self, lamb, eps=1e-20):
        """ Generate the continuum spectrum

        Parameters
        ----------
        lamb: ndarray, dtype=float
            wavelength to generate the spectrum
            expected Angstrom units

        eps: float, optional
            precision in the interpolations

        Returns
        -------
        fneb: ndarray, dtype=float
            nebular continuum normalized to NHI = 1.
            [ in ergs/s/AA/NHI ]
            (assuming no escaping photons)
        """
        u_lamb = val_in_unit('lamb', lamb, 'angstrom')
        _lamb = u_lamb.magnitude

        # continuum
        t       = Table(self.continuumFile, dtype='fits')
        HeI     = float(t.header['HEI'])
        HeII    = float(t.header['HEII'])
        alphaTe = float(t.header['ALPHATE'])

        # interpolation in x=1/lamb and y=log(gamma)
        x = 1. / _lamb
        x0 = 1. / t['lambda']

        ind         = np.where(_lamb > t['lambda'][-1])
        y           = np.log10(t['gamma1'] + eps)
        # interp requires xp to be increasing
        log_g1      = interp(x, x0[::-1], y[::-1], left=0., right=0.)
        if len(ind[0]) > 0:
            # -1.9: gaunt factor ~ nu**(-0.1) at long wavelength.
            log_g1[ind] = y[-1] + 0.1 * np.log10(x0[-1] / x[ind])
        y           = np.log10(t['gamma2'] + eps)
        log_g2      = interp(x, x0[::-1], y[::-1], left=0., right=0.)
        if len(ind[0]) > 0:
            log_g2[ind] = y[-1] + 0.1 * np.log10(x0[-1] / x[ind])
        y           = np.log10(t['gamma3'] + eps)
        log_g3      = interp(x, x0[::-1], y[::-1], left=0., right=0.)
        if len(ind[0]) > 0:
            log_g3[ind] = y[-1] + 0.1 * np.log10(x0[-1] / x[ind])
        y           = np.log10(t['gamma4'] + eps)
        log_g4      = interp(x, x0[::-1], y[::-1], left=0., right=0.)
        if len(ind[0]) > 0:
            log_g4[ind] = y[-1] + 0.1 * np.log10(x0[-1] / x[ind])
        del ind, y

        # light speed in Angstroms/second
        c_ang = float(unit['c'].to('AA/s').magnitude)

        fneb  = 10 ** log_g1 + 10 ** log_g2 + HeI * 10 ** log_g3 + HeII * 10 ** log_g4
        fneb *= 1e-40 * c_ang / alphaTe * x ** 2

        del x, x0, log_g1, log_g2, log_g3, log_g4
        del t

        return fneb

    def _get_lines_(self):
        """
        Returns wavelengths and Energy of emission lines normalized to NHI = 1
        and no escaping photons line strength relative to Hbeta (first line).
        """
        t       = Table(self.linesFile, dtype='fits')
        alphaTe = float(t.header['ALPHATE'])
        lamb    = t['lambda']
        flines  = 1.244e-25 * t['gline'] / alphaTe

        del t

        return lamb, flines

    def gen_spectra(self, lambin, with_units=False):
        """ Generate a photon normalized nebular spectrum

        On top of adding the continuum component, for each emission line, we
        compute where it should lie on the continuum and spread the energy on a
        triplet of points, single point or a couple enclosing the position.

        Parameters
        ----------
        lambin: ndarray, dtype=float
            initial wavelengths to compute
            [in Angstroms]

        with_units: bool
            if set, return results with units

        Returns
        -------
        l: ndarray, dtype=float
            wavelength values revised from the input array to include emission lines
            in Angstroms

        f: ndarray, dtype=float
            final associated fluxes
            [ in erg/s/AA/NHI/cm^2 ]
        """
        u_lamb = val_in_unit('lambin', lambin, 'angstrom')
        _lamb = u_lamb.magnitude

        # t_young = 10 Myr?

        # Compute emission spectrum
        # Get lines and associated lambda values
        le, Ee = self._get_lines_()
        ind = np.where( (le <= _lamb.max()) & (le >= _lamb.min()) )
        le = le[ind]
        Ee = Ee[ind]

        # Build a spectrum including lamb and le wavelengths by
        # interpolation. Hence generate a new lamb list
        lamb = np.hstack((_lamb, le))
        lamb.sort()
        lamb = np.unique(lamb)

        # Get the continuum
        # Lsun = float(units.lsun.rescale('erg/s'))
        fcont = self._get_continuum_(_lamb[:] * unit['angstrom'])
        flux  = interp(lamb, _lamb, fcont, left=0., right=0.)

        # for each emission line, compute where it should lie on the
        # continuum and spread the energy on a triplet of points.single point or a couple
        # enclosing the position.
        for lk in range(len(le)):
                # exact match
                jl = np.where(lamb == le[lk])[0]
                jl0 = jl - 1
                jl1 = jl + 1
                if jl0 > 0:
                    l0 = lamb[jl0]
                else:
                    l0 = lamb[0] - (lamb[1] - lamb[0])
                if jl1 < len(lamb):
                    l1 = lamb[jl1]
                else:
                    l1 = lamb[-1] - (lamb[-1] - lamb[-2])

                # Energy of the line is spread over l0, l, l1
                flux[jl] += Ee[lk] / (l1 - l0) * 2.0

        del Ee, le, fcont

        if with_units:
            unit.add_unit('NHI', 1)
            return lamb * unit['AA'], flux * unit['erg/s/AA/NHI']
        else:
            return lamb, flux

# backward comp. deprecated
# use Pegase instead for naming consistency
pegase = Pegase
