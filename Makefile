DIRS = include

.PHONY: clean all all-but-libs build libs help

.SILENT: help

define ANNOUNCE_HELP
Pypegase makefile 

Not much to say for now
Will provide common task to clean and install pyPegase

targets:

build -- make pyx extensions
clean -- clean the directory
endef

export ANNOUNCE_HELP

help:
	@echo "$$ANNOUNCE_HELP"

all: clean build libs

all-but-libs: clean build

clean: 
	find . -name '*pyc' -exec rm -f {} \; >> /dev/null
	find . -name '__pycache__' -exec rm -rf {} \; >> /dev/null
	rm -f *.so

build:
	for d in $(DIRS); do (cd $$d && $(MAKE) build ); done

libs: 
	python getlibs.py
