"""
DEMO - EXAMPLES
"""
import pylab as plt

from . import imf
from . import isochrone
from . import nebular
from . import stellib
from .ssp import continuous, discrete, fast_discrete
from .tools import figrc, timeit
from .config import __NTHREADS__

# setup figures
figrc.ezrc(18)

age = 1e8
Z = 0.02
fracNeb = 0.5
Nstars = int(1e6)

# Computations
m_continuous = True
m_discrete = False
m_fast_discrete = True

# select an IMF
oIMF     = imf.Salpeter()
# Select isochrone set
oISO     = isochrone.pegase()
# oISO     = isochrone.padova2010()
# Nebular component
oNEB     = nebular.pegase()
# Spectral library
oSL      = stellib.BaSeL()
# oSL      = stellib.Elodie()


if m_continuous:
    with timeit('continuous SSP generation'):
        l0, s0, p0 = continuous(age, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb, nthreads=__NTHREADS__, inLsun=False)

if m_discrete:
    with timeit('discrete SSP generation'):
        ln, sn, pn = discrete(Nstars, age, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb, nsamp=1, nthreads=__NTHREADS__)

if m_fast_discrete:
    with timeit('fast-discrete SSP generation'):
        lp, sp, pp = fast_discrete(Nstars, age, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb, nsamp=1, nthreads=__NTHREADS__)

# Plot the final spectrum (w/ and w/o nebular component)
plt.figure()
ax = plt.subplot(111)
if m_continuous:
    ax.loglog(l0, s0)
if m_discrete:
    ax.loglog(ln, sn / pn['Mtot'])
if m_fast_discrete:
    ax.loglog(lp, sp / pp['Mtot'])

# s = oISO._get_isochrone(1e9, 0.02)
# r = oIMF.get_enclosed_mass(10 ** min(s['logM']), 10 ** max(s['logM']))/oIMF.get_enclosed_mass(0.1, 120.)
# ax.loglog(l0, 1./r * s0)

ax.set_xlabel(r'Wavelength [$\rm{\AA}$]')
if p0['inLsun']:
    ax.set_ylabel(r'Flux [erg/s/$\rm{\AA}$/L$_\odot$/M$_\odot$]')
    ax.set_ylim(1e-7, 1.)
else:
    ax.set_ylabel(r'Flux [erg/s/$\rm{\AA}$/M$_\odot$]')
    ax.set_ylim(1e26, 1e36)
ax.set_xlim(800, 5e4)
# ax.set_xlim(500, 7000)
# ax.set_ylim(1e-10, 1e-3)

plt.show()
