""" converting old isochrone pegase lib to new pegase lib """

import numpy as np
import tables
from tools.pbar import Pbar
from io.simpletable import SimpleTable

orig = 'libs/pegase.iso.hd5'
cols = 'logL logM logT logg'.split()


def prepare_dict():
    d = {}
    for c in cols:
        d.setdefault(c, [])

    d.setdefault('age', [])
    d.setdefault('Z', [])

    return d


def flatten_dict(d):
    for k in d:
        d[k] = np.hstack(d[k])


def append_table(d, tab):
    _t = tab[:]
    for c in cols:
        d[c].append(_t[c])
    d['age'].append(np.ones(len(_t)) * tab.attrs['time'] * 1e6)
    d['Z'].append(np.ones(len(_t)) * tab.attrs['metal'])


if __name__ == '__main__':
    f = tables.open_file(orig)
    groups = f.root._v_groups.keys()

    d = prepare_dict()

    for grp in Pbar(desc='All').iterover(f.root._v_groups.keys()):
        _grp = f.getNode('/' + grp)
        for tab in Pbar(desc=grp).iterover(list(_grp._f_walknodes())):
            append_table(d, tab)

    flatten_dict(d)

    t = SimpleTable(d)
