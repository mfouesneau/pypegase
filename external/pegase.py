""" Tools for importing PEGASE files """

import numpy as np
from ..io import Table
from ..io.fits import pyfits


def _readLine(f, nlines=1, cols=None, convert=None):
    """
    Read a given lines from a file stream
    and optionaly convert field values into given format sequence

    Parameters
    ----------
    f: stream buffer
        opened file or buffer to read from

    nlines: int, optional
        number of lines to read

    cols: sequence(int), optional
        columns to read in. (default: all)

    convert: sequence, optional
        sequence of types to read in the values
        default str for all columns

    Returns
    -------
    vals: sequence
        sequence of values read from the file
        sequence of sequences if multiple lines were read.
    """
    if nlines < 1:
        return ()
    if convert is None:
        convert = [str]
    if cols is None:
        cols = np.arange(np.size(convert))

    data = {}
    for k in cols:
        data[str(k)] = []

    for il in range(nlines):
        l = f.readline()
        l = l.split()
        for k in range(np.size(cols)):
            data[str(cols[k])].append(convert[k](l[k]))

    if nlines == 1:
        return tuple([ data[str(k)][0] for k in cols ])
    else:
        return tuple([ data[str(k)] for k in cols ])


def _readBlock(f, nvalues=0, convert=None):
    """
    Read a given number of values in a file stream
    optionaly it can also convert it into a given format

    Parameters
    ----------
    f: stream buffer
        opened file or buffer to read from

    nvalues: int
        number of values to read in

    convert: type or callable, optional
        type to read in the values
        default str

    Returns
    -------
    vals: ndarray
        array of values
    """
    if nvalues == 0:
        return
    data = []

    s = len(data)  # which here means 0!

    while s < nvalues:
        l = f.readline().split()
        data = np.hstack((data,l))
        s = len(data)

    if convert is None:
        return np.array(data)
    else:
        return np.array([ convert(d) for d in data ])


def read_pegase():
    d = 'stel_lib_dir/'
    name = 'stellib_BaSeL_2.2_Rauch.fits'

    f_wave = open(d + 'stellar_libraries_wavelengths.dat')
    nw     = _readLine(f_wave, 1, convert=[int])[0]
    wave   = _readBlock(f_wave, nw).astype(float)
    f_wave.close()

    f_root = open(d + 'list_stellar_libraries.dat')
    nw     = _readLine(f_root, 1, convert=[int, int])
    l      = _readBlock(f_root, sum(nw))
    f_root.close()

    nw = len(wave)

    def extract(fk):
        f_fk = open(d + fk)
        ns, z = _readLine(f_fk, 1, convert=[int, float])
        # logT, logg, NHI, NHeI, NHeII
        tgz   = _readBlock(f_fk, ns * 5).astype(float)
        tgz   = tgz.reshape((ns, 5))
        tgz   = np.vstack([ tgz.T, np.asarray([ z ] * ns) ]).T
        s = [_readBlock(f_fk, nw).astype(float) for k in range(ns) ]
        f_fk.close()
        return tgz, np.asarray(s)

    r = map(extract, l)
    tgz   = np.vstack([ k[0] for k in r ])
    specs = np.vstack([ k[1] for k in r ])

    pars = ['Teff', 'logG', 'NHI', 'NHeI', 'NHeII', 'Z']
    d    = { pars[k]:tgz[:,k] for k in range(len(pars)) }

    t1    = Table(d)

    t1.header['EXTNAME'] = 'TGZ'
    t1.setUnit('Teff', 'log(K)')
    t1.setUnit('logG', 'log(g/s**2)')
    t1.header['COMMENT'] = 'Stellib. from Pegase.2x: BaSeL 2.2 + Rauch'

    t2    = Table( {'BFIT': wave} )
    t2.header['EXTNAME'] = 'WCA'
    t2.setUnit('BFIT', 'AA')

    pyfits.writeto(name, specs)
    t1.write(name, append=True)
    t2.write(name, append=True)
