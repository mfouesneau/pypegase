pyPEGASE v1.0
=============

**This user-guide attempts to give you a quick start, and to help you understand the structure of the code.**


A word about Pegase
-------------------

Pegase is a code aiming at computing synthetic evolutive optical spectra of galaxies (Fioc+1997). Initially written in Fortran 77 
it evolved into Fortran 90, and branched into multiple versions: Pegase.2, Pegase-HR, and Pegase.2n.

Pegase.2n was designed to compute synthetic spectra of low-mass cluster while especially modeling the stochastic regime of the low-mass regime. Further development of Pegase are focused on spectral evolution of galaxies and thus would no longer be interesting in star cluster simulations. As such, we decided to simplify and clean the code from galaxy synthesis to keep only cluster specific routines. In the meantime, our collaborators were generating a lot of interest in being able to change the ingredients of the models such as spectral library or isochrones. Ingredient flexibility was far from trivial to implement in the initial Fortran code and the decision has been made to migrate to python in order to offer such feature.


GIT quick help
--------------

Step 1 to ...  are only for the first copy of the code.

Fork the master repository from the website interface.

1. Clone your version onto your computer

```    
    git clone git@gitlab.com:<user>/pypegase.git
```

2. configure upstream source for later updates with main repository
   
```
    git remote add upstream git@gitlab.com:mfouesneau/pypegase.git
```
    
3. As a verification, there should be a upstream definition in 

```
    git remote -v
```

4. Update from upstream

```
    # get the upstream data (it sets you into this branch)
    git fetch upstream
    # return to your local version
    git checkout master   
    # merge locally (with the current active branch, therefore prev. command)
    git merge upstream/master
```

5. push all local modifications (to your version)

```
    git commit
```
    
6. request to merge to the upstream through the website.



Contents of the directory
-------------------------

The tarfile you have unpacked contains the following:

	tests/				contains different unit tests
		bineffect.py		binning effect on the fast discrete generation of models	
	tools/				common various tools 
		common
		decorators
		figure
		mytables
		
		
	config.py			defines some default variables values
	extinction.py			Extinction models (python classes)
	imf.py				(Initial) Mass functions (python classes)
	isochrone.py			Isochrone models (python classes)
	nebular.py			Nebular emission models (python classes)
	photometry.py			Manipulation of photometric filters and convolutions
	__init__.py			python package definition
	


Setup
---------

Setup assumes python is installed on your machine.  
**Important Note**: pyPEGASE  has been developed and tested with python 2.7.
_Future development will make it python 2.6 and 3 compliant._

Required Dependencies
-----------------------
All dependencies can be installed using `easy_install`, `pip`,...

 * Standard python libraries in the scientific community:
        `pyfits`          handles fits format
        `tables`          handles HDF5 formats (mainly for spectral in/output)
        `numpy`           numerical libraries 
        `scipy`           numerical libraries 
        `matplotlib`      making figures

* Not so standard libraries: None

**note**: quantities was replaced by a smaller package:pint (included)

Optional Dependencies
---------------------
        
Optional libraries are not requested but highly recommended to take advantage of the full speed of the code.

        `cython`          Used to speed up parts of the code. If not present, the code will fallback into pure python
                          version.  (speeds up computations by ~20%)


Customizations
---------------

* Run `make` to make use of cython code if cython is present.

* Edit `config.py` to customize the way you wish to run the codes.



Quick Start
----------------

**SSP Generation Description**

A Single Stellar Population is a co-eval population, which means: 

1. Stellar masses are drawn from a given mass function (dN/dM).

2. Each mass, m, is associated with a temperature Teff, a gravity log(g), and a luminosity L, through an isochrone (t=age, Z=metallicity).

3. Each star referenced by (m, Z, log(Teff), log(g), log(L) ) is then associated with a spectral contribution ( lambda, F(lambda) ).
4. The integrated SED/spectrum is the sum of the individual stellar contributions.

The integrated stellar SED/spectrum can subsequently be modified, e.g. by extinction or via the reprocessing of Lyman continuum emission into nebular emission (continuum + lines). 


**Calculation Modes**

Three modes are available to generate an SSP: continuous, fast-discrete,
discrete. 

* _continuous_: assumes a continuously populated mass function. This is a good representation only for very massive stellar populations (strictly speaking for an infinite number of stars). This mode also represents mean properties of SSPs (beware: these mean properties are very unlikely, or even forbidden, for any single low mass cluster).The results computed in this mode are normalized to 1Msun.

* _discrete_: assume a discrete sampling of the mass function. The results are provided for a given number of stars (user-defined). 
* _fast-discrete_: like a discrete computation assumes a discrete sampling of the mass function.  The fast discrete mode approximates the spectral contribution of each star in order to speed up calculations (less than 3% error using the default values).


**Shopping centers: Where to find the ingredients?**

The code is organized in multiple sub-packages, each of which collects one ingredient.
       
* imf.py         defines the mass functions
	Kennicutt 
	Kroupa2001 
	Kroupa93 
	MillerScalo 
	Salpeter 
	Scalo86 
	Scalo98 

* isochrone.py   interface isochrone libraries

* stellib.py     interface spectral libraries

* nebular.py     compute nebular components

* extinction.py  provides extinction laws
	Calzetti et al, 2000
	Cardelli et al, 19xx
	Fitzpatrick, 1999
	Gordon et al., 2003, SMC bar
	mixture of Fitzpatrick99 and Gordon03_SMCBar


=== Quick start by example : 3 steps to build one "continuous" population ===
All examples below are in demo.py (which also includes some figure scripts)

1. We need to define the population properties and ingredients. We select
    * age and metallicity:
        > age = 1.e6    # in yr
        > Z   = 0.02   # solar
    * an IMF, for instance from Kroupa (2001):
        > import imf
        > oIMF = imf.Kroupa2001()
    * a set of isochrones, Padova 2010 for instance:
        > import isochrone
        > ISO = isochrone.padova2010() 
    * a spectral library, here BaSeL (Lejeune+1998):
        > import stellib
        > oSL  = stellib.BaSeL()
    * a prescription for nebular emission (currently only the prescription 
      from Pegase.2 is available, see Fioc+1997)
        > import nebular
        > oNEB     = nebular.pegase()
        > fracNeb = 1.  # fraction of the ionizing photons that will
                        # be reprocessed into nebular emission

2. So far we did not select a computation mode. Let's do it now:
        > from ssp import continuous

3. Launch the actual computations:
        > l, s, p = continuous(age, Z, oIMF, oISO, oSL, oNEB, fracNeb=fracNeb)

        s    provides the integrated spectrum of the population.  [AL- array??]
        l    contains the wavelengths at which the spectrum is provided.
        p    is a dictionary that collects the internal properties 
             and intermediate steps of the computations.

Plot the result with your favourite python tool, for instance:
  > import pylab
  > pylab.plot(l,s)
Examine p:
  > print p.keys()


Some details, if you want to do it by hand instead of using a black box:

3.1 Selected isochrone (t,Z) in the continuous prescription (this prescription 
    imposes the isochrone sampling too.)
        > stars = oISO._get_continuous_isochrone( t, Z )
        Note:  the variable 'stars' contains all the needed the quantities
                (logT, logg, logL, Z, M, and more)

3.2 Compute the contribution of each bin of mass with respect to the IMF:
expected fraction of stars: dN = imf(M) * dM
        > dN = oIMF.nstars1 ( stars['logM'], dlogm = stars['dlogm'] )
        Note:  nstars1 does the computations in log mass (whereas nstars)

3.3 Interpolate the spectral library and compute the weight of each library star
 in the integrated spectrum of the isochrone population.
       > r = oSL.interpMany(stars['logT'], stars['logg'], Z, stars['logL'], weights = dN )

3.4 Sum the contributions
        > s0 = oSL.genSpectrum(r)
        > l0 = oSL.wavelength

3.5 Reprocess hydrogen ionizing photons NHI into nebular emission:
        > NHI  = oSL.genQ('NHI', r)
	> l, s = oNEB.add_emission(l0, s0, NHI, fracNeb)

Units: fluxes are in Lsun/AA/1Msun (AA=Angstroem, Lsun=bolometric luminosity of the Sun).
(keyword inLsun of genSpectrum is available to switch to ergs/s/AA/1Msun).


=== Quick start by example: 3 steps to build one "discrete" population ===
All examples below are in demo.py (which also includes some figure scripts)

1. We need to define the population properties and ingredients. We select
    * number of stars in the population
        > Nstars = int(1e5)
    * age and metallicity:
        > age = 1.e6    # in yr
        > Z   = 0.02    # solar
    * an IMF, for instance from Kroupa (2001):
        > import imf
        > oIMF = imf.Kroupa2001()
    * a set of isochrones, Padova 2010 for instance:
        > import isochrone
        > oISO = isochrone.padova2010() 
    * a spectral library, here BaSeL (Lejeune+1998):
        > import stellib
        > oSL  = stellib.BaSeL()
    * a prescription for nebular emission (currently only the prescription 
      from Pegase.2 is available, see Fioc+1997)
        > import nebular
        > oNEB     = nebular.pegase()
        > fracNeb = 1.  # fraction of the ionizing photons that will
                        # be reprocessed into nebular emission
    * a number of realizations can be set to generate multiple stochastic outputs
	> nsamp = 4

2. Launch the actual computations:
        > from ssp import discrete as get_ssp
	> l, s, p = get_ssp(Nstars, age, Z, oIMF, oISO, oSL, oNEB,
			fracNeb=fracNeb, dlogm=0.01, nsamp=nsamp)

        s    provides the integrated spectrum of the population.  [AL- array??]
        l    contains the wavelengths at which the spectrum is provided.
        p    is a dictionary that collects the internal properties 
             and intermediate steps of the computations.

Some details, if you want to do it by hand instead of using a black box:

2.1 Draw individual masses from an IMF
	> masses = oIMF.random(Nstars)

2.2 Populate an isochrone (t,Z) with the given masses 
	> stars = oISO._get_isochrone(age, Z, masses = masses)
        Note:  the variable 'stars' contains all the needed the quantities to
	define a population of stars (logT, logg, logL, Z, M, and more)

_The next few steps are then identical to the continuous or fast discrete modes_

2.3 Interpolate the spectral library and compute the weight of each library star
 in the integrated spectrum of the isochrone population.
       > r = oSL.interpMany(stars['logT'], stars['logg'], Z, stars['logL'])

2.4 Sum the contributions
        > s0 = oSL.genSpectrum(r)
        > l0 = oSL.wavelength

2.5 Reprocess hydrogen ionizing photons NHI into nebular emission:
        > NHI  = oSL.genQ('NHI', r)
	> l, s = oNEB.add_emission(l0, s0, NHI, fracNeb)


=== Quick start by example: 3 steps to build one "fast-discrete" population ===
All examples below are in demo.py (which also includes some figure scripts)

1. We need to define the population properties and ingredients. We select
    * number of stars in the population
        > Nstars = int(1e5)
    * age and metallicity:
        > age = 1.e6    # in yr
        > Z   = 0.02    # solar
    * an IMF, for instance from Kroupa (2001):
        > import imf
        > oIMF = imf.Kroupa2001()
    * a set of isochrones, Padova 2010 for instance:
        > import isochrone
        > oISO = isochrone.padova2010() 
    * a spectral library, here BaSeL (Lejeune+1998):
        > import stellib
        > oSL  = stellib.BaSeL()
    * a prescription for nebular emission (currently only the prescription 
      from Pegase.2 is available, see Fioc+1997)
        > import nebular
        > oNEB     = nebular.pegase()
        > fracNeb = 1.  # fraction of the ionizing photons that will
                        # be reprocessed into nebular emission

2. Mode selection: the only line that actually differs from the discrete mode
        > from ssp import fast_discrete as get_ssp

    * Optionally you can run a series of realizations by setting the nsamp variable 
	> nsamp = 4

3. Launch the actual computations:
	> l, s, p = get_ssp(Nstars, age, Z, oIMF, oISO, oSL, oNEB,
			fracNeb=fracNeb, dlogm=0.01, nsamp=nsamp)

        s    provides the integrated spectrum of the population.  [AL- array??]
        l    contains the wavelengths at which the spectrum is provided.
        p    is a dictionary that collects the internal properties 
             and intermediate steps of the computations.


=== Customize your runs ===

* Other inputs provided with this package :
 - In imf.py, you will find a variety of IMFs defined. Edit this file
   to add your own favourite.
 - In isochrone.py two sets of isochrones are currently available :
   isochrone.Padova2010 (Girardi+2010), and isochrone.pegase (Fioc+1997,
   based on ....).
 - In stellib.py two spectral libraries are currently available :
   stellib.BaSeL (Lejeune+1998) and stellib.Elodie (as in PegaseHR, 
   Le Borgne+2004

* Redden the output spectra :
  [TBD]


=== Produce large collections of simulations ===


=== Compute colors ===


=== For those who wish to read/edit the codes ===


=== Required acknowledgements ===

